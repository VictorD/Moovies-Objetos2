package csvFileReader;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.junit.Test;

import sistemaMoovies.Rating;

public class ratingFileReaderTest {
	
	Timestamp timestamp;
	LocalDate fecha;

	@Test
	public void testDadoUnTimestampRetornoLaVersionEnLocalDate()
	{
		
		LocalDate date1 = Instant.ofEpochSecond(881250949)
							 .atZone(ZoneId.of("UTC"))
							 .toLocalDate();
		
		LocalDate date2 = Instant.ofEpochSecond(891717742)
							 .atZone(ZoneId.of("UTC"))
							 .toLocalDate();
		
		LocalDate date3 = Instant.ofEpochSecond(891377468)
							 .atZone(ZoneId.of("UTC"))
							 .toLocalDate();
		
		LocalDate date1Esperado = LocalDate.of(1997,12,04);
		LocalDate date2Esperado = LocalDate.of(1998,04,04);
		LocalDate date3Esperado = LocalDate.of(1998,03,31);
		
		assertEquals(date1Esperado, date1);
		assertEquals(date2Esperado, date2);
		assertEquals(date3Esperado, date3);
	}

	
	
	@Test
	public void dadoUnArchivoCSVDeCalificacionesDePeliculasTransformoLosDatosAInstanciasDeClaseRatingDeMooviesYVerificoQueEstanCorrectamenteCreadas(){
		
		/** Extracto de u-data.new.csv
		 * 196 | 881250949 | 242 | 3
		   186 | 891717742 | 302 | 3
			22 | 878887116 | 377 | 1
		 * */
		
		CSVFileReader<Rating> BDRatings = new RatingFileReader("u.data.new.csv");
		List<Rating> ratings = BDRatings.readFile();
	
		LocalDate date1Esperado = Instant.ofEpochSecond(881250949)
				 .atZone(ZoneId.of("UTC"))
				 .toLocalDate();
		
		LocalDate date2Esperado = Instant.ofEpochSecond(891717742)
				 .atZone(ZoneId.of("UTC"))
				 .toLocalDate();
		
		LocalDate date3Esperado = Instant.ofEpochSecond(878887116)
				 .atZone(ZoneId.of("UTC"))
				 .toLocalDate();
		
		Rating rating1 = ratings.get(0);
		Rating rating2 = ratings.get(1);
		Rating rating3 = ratings.get(2);
		
		assertEquals(new Integer(196), rating1.idUsuario());
		assertEquals(date1Esperado, rating1.fechaDeCalificacion());
		assertEquals(242, rating1.peliculaId());
		assertEquals(new Integer(3), rating1.calificacion());
		
		assertEquals(new Integer(186), rating2.idUsuario());
		assertEquals(date2Esperado, rating2.fechaDeCalificacion());
		assertEquals(302, rating2.peliculaId());
		assertEquals(new Integer(3), rating2.calificacion());
		
		assertEquals(new Integer(22), rating3.idUsuario());
		assertEquals(date3Esperado, rating3.fechaDeCalificacion());
		assertEquals(377, rating3.peliculaId());
		assertEquals(new Integer(1), rating3.calificacion());
		
	}
}

















