package csvFileReader;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sistemaMoovies.Rating;

public class RatingIMDBPeliculaTestCase {

	RatingIMDBFileReader ratingIMDBFileReader;
	
	@Before
	public void setUp() throws Exception
	{
		ratingIMDBFileReader = new RatingIMDBFileReader();
	}

	@Test
	public void Dado_Un_Archivo_CSV_Con_Informacion_De_Ratings_De_Pelicula_Cuando_Pido_El_Rating_General_IMDB_De_la_pelicula_ID_110_En_El_Archivo_Pasado_Me_devuelve_Puntaje_3()
	{
		/**archivo u.custom.data.new.csv
		 * 196 | 881250949 | 110 | 3
		   186 | 891717742 | 110 | 3
		   22 | 878887116 | 110 | 1
           244 | 880606923 | 51 | 2
           166 | 886397596 | 346 | 1
           298 | 884182806 | 110 | 4
           115 | 881171488 | 110 | 2
           253 | 891628467 | 465 | 5 
           305 | 886324817 | 110 | 5
           
           apariciones de 110 = 6
           total puntajes de 110 = 3+3+1+4+2+5 = 18
           promedio = 18/6 = 3
           */
		//Setup
		CSVFileReader<Rating> BDRatings = new RatingFileReader("u.custom.data.new.csv");
		List<Rating> ratings = BDRatings.readFile();
		
		//excercise
		int ratingIMDB = ratingIMDBFileReader.ratingIMDBDePelicula(110, ratings);
		
		//Test
		assertEquals(3, ratingIMDB);
	}
	
	@Test
	public void dado_Un_archivo_CSV_Con_Informacion_De_Ratings_De_peliculas_Actualizo_Ratings_IMDB_De_Todas_Las_peliculas_En_El_Archivo_Y_Verifico_Que_Los_Puntajes_Sean_Correctos()
	{
		/**archivo u.custom.data.new.csv
		 * 196 | 881250949 | 110 | 3
		   186 | 891717742 | 110 | 3
		   22 | 878887116 | 110 | 1
           244 | 880606923 | 51 | 2
           166 | 886397596 | 346 | 1
           298 | 884182806 | 110 | 4
           115 | 881171488 | 110 | 2
           253 | 891628467 | 465 | 5 
           305 | 886324817 | 110 | 5
           
           apariciones de 110 = 6
           total puntajes de 110 = 3+3+1+4+2+5 = 18
           promedio = 18/6 = 3
           */
		//Setup
		CSVFileReader<Rating> BDRatings = new RatingFileReader("u.custom.data.new.csv");
		List<Rating> ratings = BDRatings.readFile();
		
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		map = ratingIMDBFileReader.cargarRatingsIMDBImportados(ratings);
		
		Integer promedioEsperado = 3;
		
		assertEquals(4, map.entrySet().size());
		assertEquals(promedioEsperado, map.get(110));
		
		
	}

}

























