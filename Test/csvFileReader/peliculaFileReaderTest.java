package csvFileReader;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import sistemaMoovies.ContenedorDePelicula;
import sistemaMoovies.Pelicula;



public class peliculaFileReaderTest {

	
	@Test
	public void testCreoUnaListaDePeliculasPorCSVYVerificoQueSuTamanhoSeaCorrecto(){
		
		CSVFileReader<ContenedorDePelicula> BDPeliculas = new PeliculaFileReader("u.item.new.csv");
		
		List<ContenedorDePelicula> peliculas = BDPeliculas.readFile();

		assertEquals(1682, peliculas.size());
		
	}
	
	
	
	@Test
	public void testCreoUnaListaDePeliculasPorCSVYVerificoQueLosDatosDeLasPeliculasSeanCorrectos(){
		
		CSVFileReader<ContenedorDePelicula> BDPeliculas = new PeliculaFileReader("u.item.new.csv");
		
		List<ContenedorDePelicula> peliculas = BDPeliculas.readFile();
		
		/**Extracto de archivo u.item.new.csv
		 * 1 | Toy Story (1995) | 01-Jan-1995 | tt0114709 | 0 | 0 | 0 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0
		   2 | GoldenEye (1995) | 1-Jan-95 | tt0113189 | 0 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 | 0
	       3 | Four Rooms (1995) | 01-jan-1995 | tt0113101 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 | 0
		 * */
		
		Pelicula pelicula1 = peliculas.get(0).pelicula();
		List<String> generosEsperados1 = new ArrayList<String>();
		generosEsperados1.add("Animation");
		generosEsperados1.add("Children's"); 
		generosEsperados1.add("Comedy");
		LocalDate fechaEsperada1 = LocalDate.of(1995, 01, 01);
		String imdbEsperado1 = new String("tt0114709");
		
		Pelicula pelicula2 = peliculas.get(1).pelicula();
		List<String> generosEsperados2 = new ArrayList<String>();
		generosEsperados2.add("Action");
		generosEsperados2.add("Adventure");
		generosEsperados2.add("Thriller");
		LocalDate fechaEsperada2 = LocalDate.of(1995, 01, 01);
		String imdbEsperado2 = new String("tt0113189");
		
		Pelicula pelicula3 = peliculas.get(2).pelicula();
		List<String> generosEsperados3 = new ArrayList<String>();
		generosEsperados3.add("Thriller");
		LocalDate fechaEsperada3 = LocalDate.of(1995, 01, 01);
		String imdbEsperado3 = new String("tt0113101");
		
		
		assertEquals(new String("Toy Story (1995)"), pelicula1.titulo());
		assertEquals(fechaEsperada1, pelicula1.fechaDeEstreno());
		assertTrue(pelicula1.listaDeGeneros().containsAll(generosEsperados1));
		assertEquals(imdbEsperado1, pelicula1.imdb());
		
		assertEquals(new String("GoldenEye (1995)"), pelicula2.titulo());
		assertEquals(fechaEsperada2, pelicula2.fechaDeEstreno());
		assertTrue(pelicula2.listaDeGeneros().containsAll(generosEsperados2));
		assertEquals(imdbEsperado2, pelicula2.imdb());
		
		assertEquals(new String("Four Rooms (1995)"), pelicula3.titulo());
		assertEquals(fechaEsperada3, pelicula3.fechaDeEstreno());
		assertTrue(pelicula3.listaDeGeneros().containsAll(generosEsperados3));
		assertEquals(imdbEsperado3, pelicula3.imdb());
		
	}
	
	
}
