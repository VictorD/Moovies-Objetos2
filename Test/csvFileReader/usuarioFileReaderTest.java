package csvFileReader;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import sistemaMoovies.Administrador;
import sistemaMoovies.ContenedorDeUsuario;
import sistemaMoovies.Usuario;

public class usuarioFileReaderTest {

	Administrador admin;
	
	@Test
	public void  testDadoUnArchivoCSVDeUsuariosCreoInstanciasDeUsuariosAPartirDeLosDatosYVerificoLaCorrectaConstruccionDeLosUsuarios()
	{
		CSVFileReader<ContenedorDeUsuario> BDUsuarios = new UsuarioFileReader("u.user.csv");
		List<ContenedorDeUsuario> usuarios = BDUsuarios.readFile();
		
		/**
		 * Extracto del archivo u.user.csv
		 * 1|24|M|technician|85711|Aaron|Poole
		   3|23|M|writer|32067|Aaron|Holland
		   4|24|M|technician|43537|Abel|Oliver
		*/
		
		Usuario user1 = usuarios.get(0).usuario();
		Usuario user2 = usuarios.get(1).usuario();
		Usuario user3 = usuarios.get(2).usuario();
		
		assertEquals(new Integer(24), user1.edad());
		assertEquals(new String("technician"), user1.ocupacion());
		assertEquals(new String("Aaron Poole"), user1.nombreCompleto());
		assertEquals(new String("85711"), user1.codigoPostal());
		assertEquals(0 , user1.amigos().size());
				
		assertEquals(new Integer(23), user2.edad());
		assertEquals(new String("writer"), user2.ocupacion());
		assertEquals(new String("Aaron Holland"), user2.nombreCompleto());
		assertEquals(new String("32067"), user2.codigoPostal());
		assertEquals(0, user2.amigos().size());
		
		assertEquals(new Integer(24), user3.edad());
		assertEquals(new String("technician"), user3.ocupacion());
		assertEquals(new String("Abel Oliver"), user3.nombreCompleto());
		assertEquals(new String("43537"), user3.codigoPostal());
		assertEquals(0, user3.amigos().size());
	
	}

}
