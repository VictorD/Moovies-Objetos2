package sistemaMoovies;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class NivelUsuarioTestCase 
{
	NivelUsuario nivelUserPrueba;
	@Mock Moovies unaMoovieDePrueba;
	@Mock Usuario usuario1;
	@Mock Usuario usuario2;
	@Mock Pelicula peliculaLosMuertos;
	@Mock Pelicula pelicula2;
	@Mock Pelicula pelicula3;
	@Mock Rating rating1;
	@Mock Rating rating2;
	
	@Before
	public void setUp() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		nivelUserPrueba	= new NivelUsuario(unaMoovieDePrueba);
		
	}
	
	@Test
	public void siAUnNivelUsuarioSeLePideBuscarAlUsuarioJosePerezYEstaLoRetorna()
	{
		//Setup
		when(unaMoovieDePrueba.buscarUsuario("Jose", "Perez")).thenReturn(usuario1);
		when(usuario1.nombreCompleto()).thenReturn("Jose Perez");
		
		//Test
		assertEquals("Jose Perez", nivelUserPrueba.buscarUsuario("Jose", "Perez").nombreCompleto());
	}
	
	@Test (expected=RuntimeException.class)
	public void siAUnNivelUsuarioSeLePideBuscarAlUsuarioJosePerezYNoEstaLevantaUnaExcepcion()
	{
		//Setup
		when(unaMoovieDePrueba.buscarUsuario("Jose", "Perez")).thenThrow(new RuntimeException());
		
		//Test
		assertEquals("Jose Perez", nivelUserPrueba.buscarUsuario("Jose", "Perez").nombreCompleto());
	}
	
	@Test
	public void siAUnNivelUsuarioSeLePideBuscarUnaPeliculaLosMuertosYEstaLoRetorna()
	{
		//Setup
		when(unaMoovieDePrueba.buscarPelicula("Los Muertos")).thenReturn(peliculaLosMuertos);
		when(peliculaLosMuertos.titulo()).thenReturn("Los Muertos");
		
		//Test
		assertEquals("Los Muertos", nivelUserPrueba.buscarPelicula("Los Muertos").titulo());
	}
	
	@Test (expected=RuntimeException.class)
	public void siAUnNivelUsuarioSeLePideBuscarUnaPeliculaLosMuertosYNoEstaLevantaUnaExcepcion()
	{
		//Setup
		when(unaMoovieDePrueba.buscarPelicula("Los Muertos")).thenThrow(new RuntimeException());
		
		//Test
		assertEquals("Los Muertos", nivelUserPrueba.buscarPelicula("Los Muertos").titulo());
	}
	
	@Test
	public void siUnNivelDeUsuarioSeLePideLosDiezUsuariosMasActivosDevuelveUnaListaVacia()
	{
		//Setup
		when(unaMoovieDePrueba.usuariosMasActivos()).thenReturn(new ArrayList<Usuario>());
		
		//Exercise
		assertTrue(nivelUserPrueba.diezUsuariosMasActivos().isEmpty());
	}
	
	@Test
	public void siUnNivelDeUsuarioSeLePideLosDiezUsuariosMasActivosYSoloHay2UsuariosDevuelveUnaListaCon2Usuarios()
	{
		//Setup
		ArrayList<Usuario> usuariosMasActivos	= new ArrayList<Usuario>();
		when(unaMoovieDePrueba.usuariosMasActivos()).thenReturn(usuariosMasActivos);
		
		//Exercise
		usuariosMasActivos.add(usuario1);
		usuariosMasActivos.add(usuario2);
		
		//Test
		assertEquals(2, nivelUserPrueba.diezUsuariosMasActivos().size());
	}
	
	@Test
	public void siAUnNivelDeUsuarioSeLePideLasMejoresDiezPeliculasYNoHayPeliculasDevuelveUnaListaVacia()
	{
		//Setup
		when(unaMoovieDePrueba.mejoresPeliculas()).thenReturn(new ArrayList<Pelicula>());
		
		//Exercise
		assertTrue(nivelUserPrueba.mejoresDiezPeliculas().isEmpty());
	}
	
	@Test
	public void siAUnNivelDeUsuarioSeLePideLasMejoresDiezPeliculasYSoloHayDosPeliculasDevuelveUnaListaCon2Peliculas()
	{
		//Setup
		ArrayList<Pelicula> mejoresPeliculas	= new ArrayList<Pelicula>();
		when(unaMoovieDePrueba.mejoresPeliculas()).thenReturn(mejoresPeliculas);
		
		//Exercise
		mejoresPeliculas.add(peliculaLosMuertos);
		mejoresPeliculas.add(pelicula2);
		
		//Test
		assertEquals(2, nivelUserPrueba.mejoresDiezPeliculas().size());
	}

	@Test
	public void siAUnNivelDeUsuarioSeLePreguntaSiEstaLaPeliculaLosMuertosYSiEstaRetornaTrue()
	{
		//Setup
		when(unaMoovieDePrueba.estaLaPelicula(peliculaLosMuertos)).thenReturn(true);
		
		//Test
		assertTrue(nivelUserPrueba.estaLaPelicula(peliculaLosMuertos));
	}
	
	@Test
	public void siAUnNivelDeUsuarioSeLePreguntaSiEstaLaPeliculaLosMuertosYNoEstaRetornaFalse()
	{
		//Setup
		when(unaMoovieDePrueba.estaLaPelicula(peliculaLosMuertos)).thenReturn(false);
		
		//Test
		assertFalse(nivelUserPrueba.estaLaPelicula(peliculaLosMuertos));
	}
	
	@Test
	public void dadoUnNivelDeUsuariosSiSePideLasPeliculasCalificadasPorUnUsuarioDevuelveUnaListaConLasPeliculasQueCalifico()
	{
		//setup
		ArrayList<Rating> unaListaDeRatings	= new ArrayList<Rating>();
		when(usuario1.ratings()).thenReturn(unaListaDeRatings);
		when(rating1.peliculaId()).thenReturn(1);
		when(rating2.peliculaId()).thenReturn(2);
		when(unaMoovieDePrueba.buscarPeliculaPorID(1)).thenReturn(pelicula2);
		when(unaMoovieDePrueba.buscarPeliculaPorID(2)).thenReturn(pelicula3);
		
		//exercise
		unaMoovieDePrueba.agregarPelicula(pelicula2, 1);
		unaMoovieDePrueba.agregarPelicula(pelicula3, 2);
		unaListaDeRatings.add(rating1);
		unaListaDeRatings.add(rating2);
		
		//test
		assertEquals(2, nivelUserPrueba.peliculasCalificadasPor(usuario1).size());
		assertTrue(nivelUserPrueba.peliculasCalificadasPor(usuario1).contains(pelicula2));
		assertTrue(nivelUserPrueba.peliculasCalificadasPor(usuario1).contains(pelicula3));
	}
	
	@Test
	public void dadoUnNivelDeUsuariosSiSePideLasCalificacionesDeUnUsuarioDevuelveUnaListaConLasCalificacionesQueHizo()
	{
		//setup
		ArrayList<Rating> unaListaDeRatings	= new ArrayList<Rating>();
		when(usuario1.ratings()).thenReturn(unaListaDeRatings);
		when(rating1.tituloPelicula()).thenReturn("Los Muertos");
		when(rating1.calificacion()).thenReturn(2);
		when(rating2.tituloPelicula()).thenReturn("Intocables");
		when(rating2.calificacion()).thenReturn(5);
		
		//exercise
		unaMoovieDePrueba.agregarPelicula(pelicula2, 1);
		unaMoovieDePrueba.agregarPelicula(pelicula3, 2);
		unaListaDeRatings.add(rating1);
		unaListaDeRatings.add(rating2);
		
		//test
		assertEquals(2, nivelUserPrueba.verCalificacionesDePeliculasDeUsuario(usuario1).size());
		assertTrue(nivelUserPrueba.verCalificacionesDePeliculasDeUsuario(usuario1).containsKey("Los Muertos"));
		assertTrue(nivelUserPrueba.verCalificacionesDePeliculasDeUsuario(usuario1).containsKey("Intocables"));
	}
	
	@Test
	public void dadoUnNivelDeUsuariosSiSePideCalificarUnaPeliculaPorUnUsuarioYAmbosNoTienenCalificacionesObtendranUnaCalificacion()
	{
		//Setup
		usuario1	= new Usuario("Victor", "Degano", 28, "Estudiante", "1888");
		pelicula2	= new Pelicula("Los Intocables", LocalDate.now(), "14TT89", new ArrayList<Genero>());
		unaMoovieDePrueba	= new Moovies();
		NivelUsuario nivelUserPrueba2	= new NivelUsuario(unaMoovieDePrueba);
		
		//Exercise
		unaMoovieDePrueba.agregarPelicula(pelicula2, 2);
		unaMoovieDePrueba.agregarUsuario(usuario1, 1);
		nivelUserPrueba2.calificarPeliculaPorUsuario(pelicula2, usuario1, 4);
		
		//Test
		assertEquals(true, usuario1.tieneCalificaciones());
		assertEquals(true, pelicula2.hayRatings());
	}
	
}
