package sistemaMoovies;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**{@link AdministradorTestCase} Esta clase es una clase de prueba 
 * para mostrar el correcto funcionamiento de {@link Administrador}
 * @author Victor Degano
 */
public class administradorTestCase 
{

	Administrador admin1;
	Moovies mooviePrueba;
	List<Pelicula> pelisAPasar;
	Pelicula peli1;
	Pelicula peliUnNombre2;
	ArrayList<Usuario> usuariosAPasar;
	ArrayList<String> generos;
	Usuario userVictor;
	Usuario userMauricio;
	Usuario userNadia;
	@Mock Usuario unUser;
	@Mock Pelicula unaPelicula;
	@Mock Pelicula unaPelicula1;
	
	@Before
	public void setUp() throws Exception 
	{		
		MockitoAnnotations.initMocks(this);
		mooviePrueba	= new Moovies();
		admin1			= new Administrador("Veronica", "", 0, "", "");
		mooviePrueba.agregarAdministrador(admin1);
	}
	
	@Test
	public void siAUnAdministradorNuevoSeLePideSuNombreLoDevuelve() 
	{
		//Test
		assertTrue(admin1.nombre() == "Veronica");
	}

	
	@Test
	public void siAUnAdministradorDeMooviesDondeNoHayPeliculasEImporta3Peliculas_MooviesVaATener3PeliculasEnSuBaseDeDatos()
	{
//		Extracto de peliculasPrueba.csv
//		1 | Toy Story (1995) | 01-Jan-1995 | tt0114709 | 0 | 0 | 0 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0
//		2 | GoldenEye (1995) | 01-Jan-1995 | tt0113189 | 0 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 | 0
//		3 | Four Rooms (1995) | 01-Jan-1995 | tt0113101 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 | 0
		
		//Exercise
		admin1.importarPeliculas("peliculasPrueba.csv");
		
		//Test
		assertTrue(mooviePrueba.peliculas().size() == 3);
	}

	@Test
	public void siAUnAdministradorDeMooviesDondeHayUnaPeliculaYSeQuiereImportarLaMismaPeliculaNoLoAgrega()
	{
		//Setup
		pelisAPasar	= new ArrayList<Pelicula>();
		//unaPelicula	= new Pelicula("unNombre1", LocalDate.of(2013, 9, 17), "tt852465", generos);

		//Exercise
		mooviePrueba.agregarPelicula(unaPelicula, 1);
		admin1.importarPeliculas("peliculasPrueba1.csv");
		
		//Test
		assertEquals(1, mooviePrueba.peliculas().size());
	}

	@Test
	public void siAUnAdministradorDeMooviesDondeHayUnaPeliculaYSeQuiereImportarUnaListaDondeDeDosPeliculasDondeUnaYaEstaEnMooviesSoloAgregaLaQueNoEstaRepetida()
	{
		//Setup
		pelisAPasar	= new ArrayList<Pelicula>();
		//unaPelicula	= new Pelicula("unNombre1", LocalDate.of(2014,9,15), "tt73973", generos);
		//unaPelicula1= new Pelicula("unNombre2", LocalDate.of(1996,04,03), "ttr44333", generos);
		
		//Exercise
		mooviePrueba.agregarPelicula(unaPelicula, 1);
		admin1.importarPeliculas("peliculasPrueba2.csv");
		
		//Test
		assertEquals(2, mooviePrueba.peliculas().size());
	}

	@Test
	public void siAUnAdministradorDeMooviesDondeNoHayUsuariosEImporta3Usuarios_MooviesVaATener3UsuariosEnSuBaseDeDatos()
	{
		
		//Extracto de usuarioPrueba.CSV
//		1|24|M|technician|85711|Aaron|Poole
//		3|23|M|writer|32067|Aaron|Holland
//		4|24|M|technician|43537|Abel|Oliver
		
		//Exercise
		admin1.importarUsuarios("usuariosPrueba.csv");
		
		//Test
		assertEquals(3, mooviePrueba.usuarios().size());
	}

	@Test
	public void siAUnAdministradorDeMooviesDondeHayUnUsuarioYSeQuiereImportarElMismoUsuario_NoLoAgrega()
	{
		//Setup
		when(unUser.nombreCompleto()).thenReturn("Aaron Poole");
		when(unUser.codigoPostal()).thenReturn("85711");
		when(unUser.edad()).thenReturn(25);
		when(unUser.ocupacion()).thenReturn("technician");
		
//		Extracto de usuarioPrueba.CSV
//		1|24|M|technician|85711|Aaron|Poole

		//Exercise
		mooviePrueba.agregarUsuario(unUser, 1);
		admin1.importarUsuarios("usuariosPrueba1.csv");
		
		//Test
		assertTrue(mooviePrueba.usuarios().size() == 1);
		assertEquals("Aaron Poole", mooviePrueba.buscarUsuarioPorID(1).nombreCompleto());
		assertEquals("85711", mooviePrueba.buscarUsuarioPorID(1).codigoPostal());
		assertEquals("technician", mooviePrueba.buscarUsuarioPorID(1).ocupacion());
		assertEquals(Integer.valueOf(25), mooviePrueba.buscarUsuarioPorID(1).edad());
		assertFalse(Integer.valueOf(24) == mooviePrueba.buscarUsuarioPorID(1).edad());
	}
	
	@Test
	public void siAUnAdministradorDeMooviesDondeHayUnUsuarioYSeQuiereImportar2UsuariosDondeUnoYaEstaEnElSistema_SoloAgregaAlQueNoEsta()
	{
		//Setup
		when(unUser.nombreCompleto()).thenReturn("Aaron Poole");
		when(unUser.codigoPostal()).thenReturn("85711");
		when(unUser.edad()).thenReturn(25);
		when(unUser.ocupacion()).thenReturn("technician");
		
//		Extracto de usuarioPrueba.CSV
//		1|24|M|technician|85711|Aaron|Poole
//		3|23|M|writer|32067|Aaron|Holland
		
		//Exercise
		mooviePrueba.agregarUsuario(unUser, 1);
		admin1.importarUsuarios("usuariosPrueba2.csv");
		
		//Test
		assertEquals("Aaron Poole", mooviePrueba.buscarUsuarioPorID(1).nombreCompleto());
		assertEquals("85711", mooviePrueba.buscarUsuarioPorID(1).codigoPostal());
		assertEquals("technician", mooviePrueba.buscarUsuarioPorID(1).ocupacion());
		assertEquals(Integer.valueOf(25), mooviePrueba.buscarUsuarioPorID(1).edad());
		assertFalse(Integer.valueOf(24) == mooviePrueba.buscarUsuarioPorID(1).edad());
	
		assertEquals("Aaron Holland", mooviePrueba.buscarUsuarioPorID(3).nombreCompleto());
		assertEquals("32067", mooviePrueba.buscarUsuarioPorID(3).codigoPostal());
		assertEquals("writer", mooviePrueba.buscarUsuarioPorID(3).ocupacion());
		assertEquals(Integer.valueOf(23), mooviePrueba.buscarUsuarioPorID(3).edad());
		
		assertEquals(2, mooviePrueba.usuarios().size());
	}

	@Test
	public void siUnAdministradorDeMooviesDondeNoHayPeliculasNiUsuariosQuiereImportarRatingsNoImportaNada()
	{
//		Extracto de ratingPrueba.csv
//		196 | 881250949 | 242 | 3
//		186 | 891717742 | 302 | 3
		
		//Exercise
		admin1.importarRatings("ratingsPrueba.csv");
		
		//Test
		assertFalse(mooviePrueba.hayRatings());
	}
	
	@Test
	public void siUnAdministradorDeMooviesQuiereImportarRatingsYNoHayPeliculasPeroSiUsuariosNoImportaNada()
	{
		//Setup
		userVictor	= new Usuario("Victor", "Degano", 20, "", "");
		
//		Extracto de ratingPrueba.csv
//		196 | 881250949 | 242 | 3
//		186 | 891717742 | 302 | 3
		
		//Exercise
		mooviePrueba.agregarUsuario(userVictor, 196);
		admin1.importarRatings("ratingsPrueba.csv");
		
		//Test
		assertFalse(mooviePrueba.hayRatings());
	}
	
	@Test
	public void siUnAdministradorDeMooviesQuiereImportarRatingsYNoHayUsuariosPeroSiPeliculasNoImportaNada()
	{
		//Setup
		ArrayList<Genero> unosGeneros	= new ArrayList<Genero>();
		peli1							= new Pelicula("Ranger", LocalDate.of(2003,02,24), "tt6788890", unosGeneros);

//		Extracto de ratingPrueba.csv
//		196 | 881250949 | 242 | 3
//		186 | 891717742 | 302 | 3
		
		//Exercise
		mooviePrueba.agregarPelicula(peli1, 242);
		admin1.importarRatings("ratingsPrueba.csv");
		
		//Test
		assertFalse(mooviePrueba.hayRatings());
	}
	
	@Test
	public void siUnAdministradorDeMooviesQuiereImportarUnRatingsDeLaUnicaPeliculaCalificadaPorElUnicoUsuarioDeMooviesLoHace()
	{
		//Setup
		ArrayList<Genero> unosGeneros	= new ArrayList<Genero>();
		peli1		= new Pelicula("Ranger", LocalDate.of(2003,02,24), "tt6788890", unosGeneros);
		userVictor	= new Usuario("Victor", "Degano", 0, "", "");
		
//		Extracto de ratingPrueba.csv
//		196 | 881250949 | 242 | 3
//		186 | 891717742 | 302 | 3
		
		//Exercise
		mooviePrueba.agregarUsuario(userVictor, 196);
		mooviePrueba.agregarPelicula(peli1, 242);
		admin1.importarRatings("ratingsPrueba.csv");
		
		//Test
		assertTrue(mooviePrueba.hayRatings());
		
	}

	@Test
	public void siUnAdministradorDeMooviesQuiereImportarAmigosAUnaMooviesSinUsuariosNoHaceNada()
	{		
//		Extracto De "amigosPrueba.csv"
//		1|20
//		1|57
		
		//Exercise
		admin1.importarAmigos("amigosPrueba.csv");
		
		//Test
		assertFalse(mooviePrueba.hayAmigos());
	}
	
	@Test
	public void siUnAdministradorDeMooviesQuiereImportarUnaListaDeUnaUnicaRelacionDeAmistadQueSonLosUnicosDosUsuariosDeMooviesLoImporta()
	{
//		Extracto De "amigosPrueba.csv"
//		1|20
//		1|57
		
		//Setup
		userVictor	= new Usuario("Victor", "Degano", 21, "", "");
		userMauricio= new Usuario("Mauricio", "Salinas", 22, "", "");
		userNadia	= new Usuario("Nadia", "Trotvil", 22, "", "");
		
		//Exercise
		mooviePrueba.agregarUsuario(userVictor, 1);
		mooviePrueba.agregarUsuario(userMauricio, 20);
		mooviePrueba.agregarUsuario(userNadia, 57);
		admin1.importarAmigos("amigosPrueba.csv");
		
		//Test
		assertTrue(mooviePrueba.hayAmigos());	
	}

}