package sistemaMoovies;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

public class UsuarioTestCase {

	@Spy Moovies moovies;
	@Mock Pelicula pelicula1;
	@Mock Pelicula pelicula2;
	@Mock Pelicula pelicula3;
	@Mock Pelicula pelicula4;
	@Mock Pelicula pelicula5;
	@Mock Pelicula pelicula6;
	@Mock Pelicula pelicula7;
	@Mock Pelicula pelicula8;
	@Mock Pelicula pelicula9;
	@Mock Pelicula pelicula10;
	@Mock Pelicula pelicula11;
	@Mock Usuario unUsuario1;
	@Mock Usuario unUsuario2;
	@Mock Usuario unUsuario3;
	@Mock Usuario unUsuario4;
	@Mock Usuario unUsuario5;
	@Mock Usuario unUsuario6;
	@Mock Usuario unUsuario7;
	@Mock Usuario unUsuario8;
	@Mock Usuario unUsuario9;
	@Mock Usuario unUsuario10;
	@Mock Usuario unUsuario11;
	Usuario usuario;
	Usuario usuario2;
	Usuario usuario3;
	Usuario usuario4;
	PeliculasCalificadasCon4OMasPorMasDeLaMitadDeAmigosQueUsuarioAunNoEvaluo metodo4OMasPorMasDeLaMitad;
	PeliculasCalificadasPor2OMasAmigosCon3OMasOrdenadasPorRatingIMDB metodo2OMasAmigosCon3OMasOrdenadasPorIMDB;
	PeliculasConMayorRatingPromedioQueAlMenosUnAmigoEvaluo metodoPeliculasCOnMayorRatingPromedioQueAlMenosUnAmigoEvaluo;
	
	@Before
	public void setUp() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		usuario 		= new Usuario("juan","Lopez",18,"estudiante","1881");
		usuario2		= new Usuario("Maria","Rodriguez",21,"administrativa","2902");
		usuario3		= new Usuario("Jose","Martinez",28,"abogado","2002");
		usuario4		= new Usuario("Claudia","Sanchez",28,"arquitecta","1888");
		usuario4.nivelDePrivilegios(new NivelUsuario(moovies));
		usuario3.nivelDePrivilegios(new NivelUsuario(moovies));
		usuario2.nivelDePrivilegios(new NivelUsuario(moovies));
		usuario.nivelDePrivilegios(new NivelUsuario(moovies));
		metodo4OMasPorMasDeLaMitad = new PeliculasCalificadasCon4OMasPorMasDeLaMitadDeAmigosQueUsuarioAunNoEvaluo();
		metodo2OMasAmigosCon3OMasOrdenadasPorIMDB = new PeliculasCalificadasPor2OMasAmigosCon3OMasOrdenadasPorRatingIMDB();
		metodoPeliculasCOnMayorRatingPromedioQueAlMenosUnAmigoEvaluo = new PeliculasConMayorRatingPromedioQueAlMenosUnAmigoEvaluo();
	}
	
	@Test
	public void unUsuarioRecienCreadoNoTieneCalificacionesYNoTieneAmigos()
	{
		assertTrue(usuario.tieneAmigos()== false);
		assertTrue(usuario.tieneCalificaciones() == false);
		
	}
	
	@Test
	public void siAUnUsuario1RecienCreadoAgregaAUnAmigo2AmbosSeTienenComoAmigos()
	{
		//Exercise
		usuario.agregarComoAmigo(usuario2);
		
		//Test
		assertTrue(usuario.tieneAmigos());
		assertTrue(usuario.amigoDe(usuario2));
		assertTrue(usuario2.amigoDe(usuario));
		
	}
	
	@Test
	public void aUnUsuarioRecienCreadoCalificaUnaPeliculaQueEstaEnMoovies()
	{
		//Setup
		when(pelicula1.imdb()).thenReturn("tt5465");
		when(pelicula1.titulo()).thenReturn("Harry Potter");
		
		//Exercise
		moovies.agregarPelicula(pelicula1, 1);
		moovies.agregarUsuario(usuario, 1);
		usuario.calificarPelicula(pelicula1, 4);
		
		//Test
		assertTrue(usuario.cantidadDeCalificaciones() == 1);
		assertTrue(usuario.cantidadDePeliculasCalificadas() == 1);
		assertTrue(usuario.misPuntajes().size()==1);
	}
	
	@Test
	public void dadoUnUsuarioSiLePidoSusDatosMeDevuelveUnaListaConSusDatos()
	{
		//setup
		ArrayList<String> datosEsperados	= new ArrayList<String>();
		datosEsperados.add("juan");
		datosEsperados.add("Lopez");
		datosEsperados.add("18");
		datosEsperados.add("1881");
		datosEsperados.add("estudiante");
		
		//Exercise
		ArrayList<String> datosDeUsuario	= usuario.misDatos();
		
		//Test
		assertTrue(datosDeUsuario.containsAll(datosEsperados));
	}
	
	@Test
	public void dadoUnUsuarioSiQuiereBuscarUnaPeliculaQueEstaEnMooviesLaConsigue()
	{
		//Setup
		doReturn(pelicula1).when(moovies).buscarPelicula("Harry Potter");
		Pelicula peliculaEncontrada;
		
		//Excercise
		peliculaEncontrada	= usuario.buscarPelicula("Harry Potter");
		
		//Test
		assertEquals(pelicula1, peliculaEncontrada);
	}
	
	@Test (expected= RuntimeException.class)
	public void dadoUnUsuarioSiQuiereBuscarUnaPeliculaQueNoEstaEnMooviesRecibeUnaExcepcion()
	{
		//Setup
		doThrow(new RuntimeException	("No se encontro la pelicula")).when(moovies).buscarPelicula("Harry Potter");
		
		//Excercise
		usuario.buscarPelicula("Harry Potter");
	}
	
	@Test
	public void dadoUnUsuarioSiQuiereBuscarAUnUsuarioQueEstaEnMooviesLoConsigue()
	{
		//Setup
		Usuario usuarioEncontrada;
		doReturn(usuario2).when(moovies).buscarUsuario("Maria", "Rodriguez");
		
		//Excercise
		usuarioEncontrada	= usuario.buscarUsuario("Maria", "Rodriguez");
		
		//Test
		assertEquals(usuario2, usuarioEncontrada);
	}
	
	@Test (expected= RuntimeException.class)
	public void dadoUnUsuarioSiQuiereBuscarAUnUsuarioQueNoEstaEnMooviesRecibeUnaExcepcion()
	{
		//Setup
		doThrow(new RuntimeException	("No se encontro al Usuario")).when(moovies).buscarUsuario("Pepe", "Cross");
		
		//Excercise
		usuario.buscarUsuario("Pepe", "Cross");
	}
	
	@Test
	public void dadoUnUsuarioConUnAmigoSiPideVerDatosDeSuAmigoLoDevuelve()
	{
		//setup
		ArrayList<String> datosEsperados	= new ArrayList<String>();
		datosEsperados.add("Maria");
		datosEsperados.add("Rodriguez");
		datosEsperados.add("21");
		datosEsperados.add("2902");
		datosEsperados.add("administrativa");
		
		//Exercise
		usuario.agregarComoAmigo(usuario2);
		ArrayList<String> datosRecibidos	= usuario.verDatosAmigo(usuario2);
		
		assertEquals(datosEsperados, datosRecibidos);
	}
	
	@Test (expected = RuntimeException.class)
	public void dadoUnUsuarioSinAmigosSiPideVerLosDatosDeUnUsuarioQueNoEsAmigoLeDaUnaExcepcion()
	{
		usuario.verDatosAmigo(usuario2);
	}
	
	@Test
	public void dadoUnUsuarioSinCalificacionesSiQuiereVerSusCalificacionesNoObtieneNinguna()
	{
		assertEquals(0, usuario.verCalificacionesDePeliculas().size());
	}
	
	@Test
	public void dadoUnUsuarioConUnaCalificacionSiQuiereVerSusCalificacionesObtieneunaSola()
	{
		//Setup
		when(pelicula1.imdb()).thenReturn("tt5465");
		when(pelicula1.titulo()).thenReturn("Harry Potter");
		
		//Exercise
		moovies.agregarPelicula(pelicula1, 1);
		moovies.agregarUsuario(usuario, 2);
		usuario.calificarPelicula(pelicula1, 5);
		
		//Test
		assertEquals(1, usuario.verCalificacionesDePeliculas().size());
	}
	
	@Test
	public void dadoUnUsuarioSiPideVerLosDatosDeUnaPeliculaLosRecibe()
	{
		//Setup
		ArrayList<String> datosRecibidos;
		ArrayList<String> datosPelicula	= new ArrayList<String>();
		datosPelicula.add("Nombre: Harry Potter");
		datosPelicula.add("Fecha de estreno: " + LocalDate.of(2001, 11, 30));
		datosPelicula.add("Generos: Aventura, Fantasia");
		when(pelicula1.informacionDePelicula()).thenReturn(datosPelicula);
		
		//Exercise
		datosRecibidos = usuario.verInformacionDePelicula(pelicula1);
		
		//Test
		assertEquals(datosPelicula, datosRecibidos);
	}
	
	@Test
	public void dadoUnUsuarioDeMooviesQuePideVerLas10MejoresPeliculasRecibeUnaListaDeLas10MejoresPeliculas()
	{
		//Setup
		ArrayList<Pelicula> mejoresPelis			= new ArrayList<Pelicula>();
		ArrayList<Pelicula> mejoresPelisResultado	= new ArrayList<Pelicula>();
		ArrayList<Pelicula> mejoresPelisEsperado	= new ArrayList<Pelicula>();
		mejoresPelis.sort(new OrdenarPeliculaPorPromedio());
		when(pelicula1.promedio()).thenReturn(10);
		when(pelicula2.promedio()).thenReturn(9);
		when(pelicula3.promedio()).thenReturn(8);
		when(pelicula4.promedio()).thenReturn(7);
		when(pelicula5.promedio()).thenReturn(6);
		when(pelicula6.promedio()).thenReturn(5);
		when(pelicula7.promedio()).thenReturn(4);
		when(pelicula8.promedio()).thenReturn(3);
		when(pelicula9.promedio()).thenReturn(2);
		when(pelicula10.promedio()).thenReturn(1);
		when(pelicula11.promedio()).thenReturn(0);
		doReturn(mejoresPelisEsperado).when(moovies).mejoresPeliculas();
		
		//Exercise
		mejoresPelis.add(pelicula1);
		mejoresPelis.add(pelicula2);
		mejoresPelis.add(pelicula3);
		mejoresPelis.add(pelicula4);
		mejoresPelis.add(pelicula5);
		mejoresPelis.add(pelicula6);
		mejoresPelis.add(pelicula7);
		mejoresPelis.add(pelicula8);
		mejoresPelis.add(pelicula9);
		mejoresPelis.add(pelicula10);
		mejoresPelis.add(pelicula11);
		mejoresPelisEsperado.addAll(mejoresPelis.subList(0, 10));
		mejoresPelisResultado	= usuario.verMejores10Peliculas();
		
		
		
		//Test
		assertEquals(10, mejoresPelisResultado.size());
		assertTrue	(mejoresPelisResultado.containsAll(mejoresPelisEsperado));
		assertTrue	(	(mejoresPelisResultado.get(0).promedio()>mejoresPelisResultado.get(9).promedio())
						&&
						(mejoresPelisResultado.get(0).promedio()>mejoresPelisResultado.get(1).promedio())
						&&
						(mejoresPelisResultado.get(9).promedio()<mejoresPelisResultado.get(8).promedio())
					);
	}
	
	@Test
	public void dadoUnUsuarioDeMooviesQuePideVerLosUsuariosMasActivosRecibeUnaListaDeLos10UsuariosMasActivos()
	{
		//Setup
		ArrayList<Usuario> masActivos				= new ArrayList<Usuario>();
		ArrayList<Usuario> masActivosResultado		= new ArrayList<Usuario>();
		ArrayList<Usuario> masActivosEsperado		= new ArrayList<Usuario>();
		masActivos.sort(new OrdenarUsuariosPorCantidadDeCalificaciones());
		when(unUsuario1.cantidadDeCalificaciones()).thenReturn(10);
		when(unUsuario2.cantidadDeCalificaciones()).thenReturn(9);
		when(unUsuario3.cantidadDeCalificaciones()).thenReturn(8);
		when(unUsuario4.cantidadDeCalificaciones()).thenReturn(7);
		when(unUsuario5.cantidadDeCalificaciones()).thenReturn(6);
		when(unUsuario6.cantidadDeCalificaciones()).thenReturn(5);
		when(unUsuario7.cantidadDeCalificaciones()).thenReturn(4);
		when(unUsuario8.cantidadDeCalificaciones()).thenReturn(3);
		when(unUsuario9.cantidadDeCalificaciones()).thenReturn(2);
		when(unUsuario10.cantidadDeCalificaciones()).thenReturn(1);
		when(unUsuario11.cantidadDeCalificaciones()).thenReturn(0);
		doReturn(masActivosEsperado).when(moovies).usuariosMasActivos();
		
		//Exercise
		masActivos.add(unUsuario1);
		masActivos.add(unUsuario2);
		masActivos.add(unUsuario3);
		masActivos.add(unUsuario4);
		masActivos.add(unUsuario5);
		masActivos.add(unUsuario6);
		masActivos.add(unUsuario7);
		masActivos.add(unUsuario8);
		masActivos.add(unUsuario9);
		masActivos.add(unUsuario10);
		masActivos.add(unUsuario11);
		masActivosEsperado.addAll(masActivos.subList(0, 10));
		masActivosResultado	= usuario.verUsuariosMasActivos();
		
		//Test
		assertEquals(10, masActivosResultado.size());
		assertTrue	(masActivosResultado.containsAll(masActivosEsperado));
		assertTrue	(	(masActivosResultado.get(0).cantidadDeCalificaciones()>masActivosResultado.get(9).cantidadDeCalificaciones())
						&&
						(masActivosResultado.get(0).cantidadDeCalificaciones()>masActivosResultado.get(1).cantidadDeCalificaciones())
						&&
						(masActivosResultado.get(9).cantidadDeCalificaciones()<masActivosResultado.get(8).cantidadDeCalificaciones())
					);
	}
	
	@Test
	public void dadoUnUsuarioSinNuevasPeliculasDeInteresSiLePidoLasPeliculasDeInteresMeDevuelveUnaListaVacia()
	{
		//Test
		assertTrue(usuario.nuevasPeliculasDeInteres().isEmpty());
	}
	
	@Test
	public void dadoUnUsuarioElCualRecibeUnaNuevaPeliculaDeInteresSiLePidoLasPeliculasDeInteresMeDevuelveUnaListaDeTamaño1()
	{
		//Exercise
		usuario.agregarPeliculaDeInteres(pelicula1);
		
		//Test
		assertEquals(1, usuario.nuevasPeliculasDeInteres().size());
		assertEquals(pelicula1, usuario.nuevasPeliculasDeInteres().get(0));
	}
	
	@Test
	public void dadoUnUsuarioElCualRecibeUnaNuevaPeliculaDeInteresQueYaLaTieneSiLePidoLasPeliculasDeInteresMeDevuelveUnaListaDeTamaño1()
	{
		//Exercise
		usuario.agregarPeliculaDeInteres(pelicula1);
		usuario.agregarPeliculaDeInteres(pelicula1);
		
		//Test
		assertEquals(1, usuario.nuevasPeliculasDeInteres().size());
		assertEquals(pelicula1, usuario.nuevasPeliculasDeInteres().get(0));
	}
	
	@Test
	public void Dado_Un_Usuario_Cuando_Le_Pido_Las_Peliculas_Calificadas_Con_Un_Puntaje_Igual_O_Mayor_al_Pedido_Obtengo_Una_Lista_De_Peliculas_Que_Obedecen_A_Esa_Condicion()
	{
		//Setup
		when(pelicula1.imdb()).thenReturn("tt4544");
		when(pelicula2.imdb()).thenReturn("tt4111");
		
		//Exercise
		moovies.agregarPelicula(pelicula1, 1);
		moovies.agregarPelicula(pelicula2, 2);
		moovies.agregarUsuario(usuario, 1);
		usuario.calificarPelicula(pelicula1, 4);
		usuario.calificarPelicula(pelicula2, 4);
		//Test
		assertEquals(2, usuario.peliculasCalificadasConPuntajeMinimo(4).size());
	}
	
	
	//METODOS DE RECOMENDACION DE PELICULAS**************************************************************
	@Test
	public void Dado_Un_Usuario_Cuando_Selecciona_Un_Metodo_de_Recomendacion_Este_Metodo_Queda_Definido_Para_El_Usuario_Hasta_Que_Decida_Cambiarlo()
	{
		//Setup
		usuario.seleccionarMetodoDeRecomendacionDePeliculas(metodo4OMasPorMasDeLaMitad);
		
		//Test
		assertNotNull(usuario.metodoDeRecomendacion());
	}
	
	//METODO DE PUNTAJE 4 O MAYOR POR MAS DE LA MITAD DE AMIGOS.
	@Test 
	public void Dado_Un_Usuario_Con_Metodo_de_Recomendacion_De_peliculas_PeliculasCalificadasCon4OMasPorMasDeLaMitadDeAmigosQueUsuarioAunNoEvaluo_Obtiene_unA_Lista_con_las_peliculas_que_se_ajustan_a_ese_metodo_De_recomendacion()
	{
		//Setup
		when(pelicula1.imdb()).thenReturn("tt4501");
		when(pelicula2.imdb()).thenReturn("tt4102");
		when(pelicula3.imdb()).thenReturn("tt4503");
		when(pelicula4.imdb()).thenReturn("tt4504");
		when(pelicula5.imdb()).thenReturn("tt4505");
				
		moovies.agregarPelicula(pelicula1, 1);
		moovies.agregarPelicula(pelicula2, 2);
		moovies.agregarPelicula(pelicula3, 3);
		moovies.agregarPelicula(pelicula4, 4);
		moovies.agregarPelicula(pelicula5, 5);
		
		moovies.agregarUsuario(usuario, 1);
		moovies.agregarUsuario(usuario2, 2);
		moovies.agregarUsuario(usuario3, 3);
		moovies.agregarUsuario(usuario4, 4);
		
		//Exercise
		usuario.seleccionarMetodoDeRecomendacionDePeliculas(metodo4OMasPorMasDeLaMitad);
		usuario.agregarComoAmigo(usuario2);
		usuario.agregarComoAmigo(usuario3);
		usuario.agregarComoAmigo(usuario4);
		
		usuario.calificarPelicula(pelicula5, 4);
		
		usuario2.calificarPelicula(pelicula1, 4);
		usuario2.calificarPelicula(pelicula2, 4);
		usuario2.calificarPelicula(pelicula3, 2);
		usuario2.calificarPelicula(pelicula5, 5);
		
		usuario3.calificarPelicula(pelicula1, 4);
		usuario3.calificarPelicula(pelicula2, 2);
		usuario3.calificarPelicula(pelicula3, 2);
		
		usuario4.calificarPelicula(pelicula1, 1);
		usuario4.calificarPelicula(pelicula2, 2);
		usuario4.calificarPelicula(pelicula3, 4);
		usuario4.calificarPelicula(pelicula5, 4);
		
		ArrayList<Pelicula> recomendadas = new ArrayList<Pelicula>();
		recomendadas = usuario.peliculasRecomendadas();
		
		//Test
		assertEquals(1, recomendadas.size());
	}
	
	//METODO EVALUADAS POR 2 O MAS AMIGOS CON PUNTAJE 3 O SUPERIOR ORDENADAS POR RATING IMDB.
	@Test
	public void dado_Un_Usuario_Con_Metodo_De_Recomendacion_De_Peliculas_PeliculasCalificadasPor2OMasAmigosCon3OMasOrdenadasPorRatingIMDB_Obtiene_Una_Lista_con_las_peliculas_que_Se_Ajustan_A_Ese_metodo()
	{
			//Setup
			when(pelicula1.imdb()).thenReturn("tt4501");
			when(pelicula2.imdb()).thenReturn("tt4102");
			when(pelicula3.imdb()).thenReturn("tt4503");
			when(pelicula4.imdb()).thenReturn("tt4504");
			when(pelicula5.imdb()).thenReturn("tt4505");
			when(pelicula1.ratingIMDB()).thenReturn(9);
			when(pelicula2.ratingIMDB()).thenReturn(6);
			when(pelicula3.ratingIMDB()).thenReturn(4);
			when(pelicula4.ratingIMDB()).thenReturn(3);
			when(pelicula5.ratingIMDB()).thenReturn(2);
			
					
			moovies.agregarPelicula(pelicula1, 1);
			moovies.agregarPelicula(pelicula2, 2);
			moovies.agregarPelicula(pelicula3, 3);
			moovies.agregarPelicula(pelicula4, 4);
			moovies.agregarPelicula(pelicula5, 5);
			
			moovies.agregarUsuario(usuario, 1);
			moovies.agregarUsuario(usuario2, 2);
			moovies.agregarUsuario(usuario3, 3);
			moovies.agregarUsuario(usuario4, 4);
			
			//Exercise
			usuario.seleccionarMetodoDeRecomendacionDePeliculas(metodo2OMasAmigosCon3OMasOrdenadasPorIMDB);
			usuario.agregarComoAmigo(usuario2);
			usuario.agregarComoAmigo(usuario3);
			usuario.agregarComoAmigo(usuario4);
			
			usuario2.calificarPelicula(pelicula1, 4);
			usuario2.calificarPelicula(pelicula2, 4);
			usuario2.calificarPelicula(pelicula3, 3);
			usuario2.calificarPelicula(pelicula5, 5);
			
			usuario3.calificarPelicula(pelicula1, 4);
			usuario3.calificarPelicula(pelicula2, 2);
			usuario3.calificarPelicula(pelicula3, 2);
			
			usuario4.calificarPelicula(pelicula1, 1);
			usuario4.calificarPelicula(pelicula2, 2);
			usuario4.calificarPelicula(pelicula3, 4);
			usuario4.calificarPelicula(pelicula5, 4);
			
			ArrayList<Pelicula> recomendadas = new ArrayList<Pelicula>();
			recomendadas = usuario.peliculasRecomendadas();
			
			//Test
			assertEquals(3, recomendadas.size());
			assertEquals(pelicula1, recomendadas.get(0));
			assertEquals(pelicula3, recomendadas.get(1));
			assertEquals(pelicula5, recomendadas.get(2));
	}

	//PELICULAS CON MAYOR PROMEDIO(5) QUE AL MENOS UN AMIGO HAYA EVALUADO
	@Test
	public void dado_Un_Usuario_Con_Metodo_De_Recomendacion_De_Peliculas_metodoPeliculasCOnMayorRatingPromedioQueAlMenosUnAmigoEvaluo_Obtiene_Una_Lista_con_las_peliculas_que_Se_Ajustan_A_Ese_metodo()
	{
		//Setup
		when(pelicula1.imdb()).thenReturn("tt4501");
		when(pelicula2.imdb()).thenReturn("tt4102");
		when(pelicula3.imdb()).thenReturn("tt4503");
		when(pelicula4.imdb()).thenReturn("tt4504");
		when(pelicula5.imdb()).thenReturn("tt4505");
		when(pelicula1.promedio()).thenReturn(5);
		when(pelicula2.promedio()).thenReturn(5);
		when(pelicula3.promedio()).thenReturn(2);
		when(pelicula4.promedio()).thenReturn(3);
		when(pelicula5.promedio()).thenReturn(2);
	
		moovies.agregarPelicula(pelicula1, 1);
		moovies.agregarPelicula(pelicula2, 2);
		moovies.agregarPelicula(pelicula3, 3);
		moovies.agregarPelicula(pelicula4, 4);
		moovies.agregarPelicula(pelicula5, 5);
		
		moovies.agregarUsuario(usuario, 1);
		moovies.agregarUsuario(usuario2, 2);
		moovies.agregarUsuario(usuario3, 3);
		moovies.agregarUsuario(usuario4, 4);
		
		//Exercise
		usuario.seleccionarMetodoDeRecomendacionDePeliculas(metodoPeliculasCOnMayorRatingPromedioQueAlMenosUnAmigoEvaluo);
		usuario.agregarComoAmigo(usuario2);
		usuario.agregarComoAmigo(usuario3);
		usuario.agregarComoAmigo(usuario4);
		
		usuario2.calificarPelicula(pelicula1, 4);
		usuario2.calificarPelicula(pelicula2, 4);
		usuario2.calificarPelicula(pelicula3, 3);
		usuario2.calificarPelicula(pelicula5, 5);
		
		usuario3.calificarPelicula(pelicula1, 4);
		usuario3.calificarPelicula(pelicula2, 2);
		usuario3.calificarPelicula(pelicula3, 2);
		
		usuario4.calificarPelicula(pelicula1, 1);
		usuario4.calificarPelicula(pelicula2, 2);
		usuario4.calificarPelicula(pelicula3, 4);
		usuario4.calificarPelicula(pelicula5, 4);
		
		
		ArrayList<Pelicula> recomendadas = new ArrayList<Pelicula>();
		recomendadas = usuario.peliculasRecomendadas();
		recomendadas = usuario.peliculasRecomendadas();
		//Test
		assertEquals(2, recomendadas.size());
		assertEquals(pelicula1, recomendadas.get(0));
		assertEquals(pelicula2, recomendadas.get(1));
	}
}

