package sistemaMoovies;

import static org.junit.Assert.*;


import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class peliculaTest 
{
	Pelicula pelicula;
	ArrayList<Genero> generos;
	Rating rating1;
	Usuario usuario1;
	Usuario usuario2;
	Moovies mooviesPrueba;
	
	@Before
	public void setUp(){
		generos = new ArrayList<Genero>();
		this.generos.add(new GeneroGeneral("Accion")); 
		this.generos.add(new GeneroGeneral("Aventura"));
		
		pelicula = new Pelicula("peli", LocalDate.of(2017,  05,  30), "tt252445", generos); 
		
		rating1 = new Rating(123, LocalDate.of(2017, 4, 26), 3, 4);
		mooviesPrueba = new Moovies();
		usuario1 = new Usuario("Maria","Rodriguez",21,"administrativa","2902");
		usuario2 = new Usuario("Jose","Martinez",28,"abogado","2002");
		usuario1.nivelDePrivilegios(new NivelUsuario(mooviesPrueba));
		usuario2.nivelDePrivilegios(new NivelUsuario(mooviesPrueba));
		
	
	}
	
	
	@Test
	public void test_creo_una_pelicula_y_verifico_que_este_correctamente_inicializada(){
		
//		List<String> generosEsperados = new ArrayList<String>();
//		generosEsperados.add("Accion");
//		generosEsperados.add("Aventura"); 
		
		LocalDate fechaEsperada = LocalDate.parse("2017-05-30");
		
		assertEquals("peli", this.pelicula.titulo());
		assertEquals(fechaEsperada, this.pelicula.fechaDeEstreno());
		assertEquals("tt252445", this.pelicula.imdb());
		assertEquals("Accion", this.pelicula.generos().get(0).nombre());
		assertEquals("Aventura", this.pelicula.generos().get(1).nombre());
	}
	
	@Test
	public void test_dada_una_pelicula_cuando_quiero_conocer_sus_votaciones_recibo_una_lista_de_calificaciones(){
		
		this.pelicula.guardarRating(rating1);
	
		assertEquals(1, this.pelicula.ratings().size());
		
	}
	
	@Test
	public void Dada_Una_Pelicula_Cuando_Pido_Su_calificacion_promedio_Y_Recibo_El_Puntaje_Indicado()
	{
		mooviesPrueba.agregarPelicula(pelicula, 1);
		mooviesPrueba.agregarUsuario(usuario1,  1);
		mooviesPrueba.agregarUsuario(usuario2,  2);
		usuario1.calificarPelicula(pelicula, 4);
		usuario2.calificarPelicula(pelicula, 2);
		
		int promedio = pelicula.promedio();
		
		assertEquals(3, promedio);
	}

	@Test
	public void Dada_Una_Pelicula_Cuando_Pido_Su_calificacion_promedio_Y_No_Tiene_Calificaciones_Recibo_El_Puntaje_0()
	{
		int promedio = pelicula.promedio();
		
		assertEquals(0, promedio);
	}
	
	@Test
	public void Dada_Una_Pelicula_Si_Le_Pido_sus_datos_me_Devuelve_Una_Lista_con_sus_datos()
	{
		//setup
		ArrayList<String> datosEsperados	= new ArrayList<String>();
		datosEsperados.add("Nombre: peli");
		datosEsperados.add("Fecha de estreno: 2017-05-30");
		datosEsperados.add("Generos: [Accion, Aventura]");
		//[ Nombre: peli, Fecha de estreno:2017-05-30, Generos:[Accion, Aventura]]
		//
		assertEquals(datosEsperados, pelicula.informacionDePelicula());
		
	}

}







