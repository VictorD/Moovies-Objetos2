package sistemaMoovies;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class ratingTest {

	
	Rating rating;
	Pelicula pelicula;
	Usuario usuario;
	
	
	@Before
	public void setUp(){
	
		//rating = new Rating(new String("nombre de usuario"), new Integer(17), new String("nombre de pelicula"), 
		//                    new String("imdb pelicula"), LocalDate.of(2017,  02,  24), new Integer(4));
		
		rating = new Rating(1, LocalDate.of(2017,  02,  24), 3, 4);
		
	}
	
	@Test
	public void test_creo_una_rating_y_verifico_que_este_correctamente_inicializado()
	{
		
		Integer idEsperado    = new Integer(1);
		
		assertEquals(idEsperado, this.rating.idUsuario());
		assertEquals(LocalDate.of(2017, 02, 24), this.rating.fechaDeCalificacion());
		assertEquals(3, this.rating.peliculaId());
		assertEquals(new Integer(4), this.rating.calificacion());
		assertEquals("", this.rating.nombreUsuario());
		assertEquals("", this.rating.tituloPelicula());
		assertEquals("", this.rating.peliculaImdb());
	}

	

}