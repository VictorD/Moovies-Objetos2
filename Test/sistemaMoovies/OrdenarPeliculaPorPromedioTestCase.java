package sistemaMoovies;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OrdenarPeliculaPorPromedioTestCase 
{
	Comparator<Pelicula> comparador;
	@Mock Pelicula peliculaA;
	@Mock Pelicula peliculaB;
	
	@Before
	public void setUp() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		comparador	= new OrdenarPeliculaPorPromedio();
	}

	@Test
	public void dadoLaPeliculaAConUnPromedioDe2YLaPeliculaBConUnPromedio0SiSeComparanRetorna1Negativo() 
	{
		//Setup
		when(peliculaA.promedio()).thenReturn(2);
		when(peliculaB.promedio()).thenReturn(0);
		
		//Test
		assertEquals(-1,comparador.compare(peliculaA, peliculaB));
	}
	
	@Test
	public void dadoLaPeliculaAConUnPromedioDe2YLaPeliculaBConUnPromedio2SiSeComparanRetorna0() 
	{
		//Setup
		when(peliculaA.promedio()).thenReturn(2);
		when(peliculaB.promedio()).thenReturn(2);

		//Test
		assertEquals(0, comparador.compare(peliculaA, peliculaB));
	}

	@Test
	public void dadoLaPeliculaAConUnPromedioDe0YLaPeliculaBConUnPromedio2SiSeComparanRetorna1() 
	{
		//Setup
		when(peliculaA.promedio()).thenReturn(0);
		when(peliculaB.promedio()).thenReturn(2);

		//Test
		assertEquals(1, comparador.compare(peliculaA, peliculaB));
	}

}
