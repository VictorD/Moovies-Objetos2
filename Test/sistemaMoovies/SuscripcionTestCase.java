package sistemaMoovies;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

public class SuscripcionTestCase 
{
	@Mock GeneroGeneral unGeneroDeAccion;
	@Mock GeneroEspecifico unGeneroDeComediaPolicial;
	@Spy Usuario unUsuarioJorge = new Usuario("", "", 0, "", "");
	@Mock Moovies unaMoovies;
	@Mock Pelicula unaPelicula;
	@Mock Pelicula unaPelicula2;
	ArrayList<Genero> generoSuscripto;
	Suscripcion unaSuscripcion;
	
	@Before
	public void setUp() 
	{
		MockitoAnnotations.initMocks(this);
		generoSuscripto	= new ArrayList<Genero>();
		generoSuscripto.add(unGeneroDeAccion);
		unaSuscripcion	= new Suscripcion(generoSuscripto, unUsuarioJorge);
	}

	@Test
	public void dadaUnaSuscripcionAlGeneroAccionHechaPorElUsuarioJorgeSiPidoElUsuarioQueHizoLaSuscripcionYElGeneroLoDevuelve() 
	{
		//Test
		assertEquals(unUsuarioJorge, unaSuscripcion.suscriptor());
		assertEquals(generoSuscripto, unaSuscripcion.generosSuscriptos());
	}

	@Test
	public void dadaUnaSuscripcionDeUnUsuarioSinPeliculasDeInteresSiSeLePideQueHagaUpdateConUnaPeliculaLaCualNoTieneNingunGeneroDeLaSuscripcionElUsuarioSigueSinTenerPeliculasDeInteres()
	{
		//Setup
		ArrayList<String> listaDeGenerosDeLaSuscripcion	=	new ArrayList<String>();
		listaDeGenerosDeLaSuscripcion.add("Accion");
		ArrayList<String> listaDeGenerosDeUnaPelicula	=	new ArrayList<String>();
		listaDeGenerosDeUnaPelicula.add("Comedia");
		when(unaPelicula.listaDeGeneros()).thenReturn(listaDeGenerosDeUnaPelicula);
		when(unGeneroDeAccion.listarGeneros()).thenReturn(listaDeGenerosDeLaSuscripcion);
		
		//Exercise
		unaSuscripcion.update(unaMoovies, unaPelicula);
		
		//Test
		assertTrue(unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().isEmpty());
		verify(unUsuarioJorge, times(0)).agregarPeliculaDeInteres(unaPelicula);
	}
	
	@Test
	public void dadaUnaSuscripcionDeUnUsuarioSinPeliculasDeInteresSiSeLePideQueHagaUpdateConUnaPeliculaLaCualTieneUnGeneroDeLaSuscripcionElUsuarioVaATenerUnaPeliculasDeInteres()
	{
		//Setup
		ArrayList<String> listaDeGenerosDeLaSuscripcion	=	new ArrayList<String>();
		listaDeGenerosDeLaSuscripcion.add("Accion");
		ArrayList<String> listaDeGenerosDeUnaPelicula	=	new ArrayList<String>();
		listaDeGenerosDeUnaPelicula.add("Accion");
		when(unaPelicula.listaDeGeneros()).thenReturn(listaDeGenerosDeUnaPelicula);
		when(unGeneroDeAccion.listarGeneros()).thenReturn(listaDeGenerosDeLaSuscripcion);
		
		//Exercise
		unaSuscripcion.update(unaMoovies, unaPelicula);
		
		//Test
		assertTrue(! unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().isEmpty());
		assertEquals(1, unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().size());
		assertTrue(unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().contains(unaPelicula));
		verify(unUsuarioJorge, times(1)).agregarPeliculaDeInteres(unaPelicula);
	}
	
	@Test
	public void dadaUnaSuscripcionAUnGeneroEspecificoDeUnUsuarioSinPeliculasDeInteresSiSeLePideQueHagaUpdateConUnaPeliculaLaCualTieneUnGeneroDeLaSuscripcionElUsuarioVaATenerUnaPeliculaDeInteres()
	{
		//Setup
		generoSuscripto	= new ArrayList<Genero>();
		generoSuscripto.add(unGeneroDeComediaPolicial);
		unaSuscripcion	= new Suscripcion(generoSuscripto, unUsuarioJorge);
		ArrayList<String> listaDeGenerosDeLaSuscripcion	=	new ArrayList<String>();
		listaDeGenerosDeLaSuscripcion.add("Comedia Policial");
		ArrayList<String> listaDeGenerosDeUnaPelicula	=	new ArrayList<String>();
		listaDeGenerosDeUnaPelicula.add("Accion");
		listaDeGenerosDeUnaPelicula.add("Comedia Policial");
		when(unaPelicula.listaDeGeneros()).thenReturn(listaDeGenerosDeUnaPelicula);
		when(unGeneroDeComediaPolicial.listarGeneros()).thenReturn(listaDeGenerosDeLaSuscripcion);
		
		//Exercise
		unaSuscripcion.update(unaMoovies, unaPelicula);
		
		//Test
		assertTrue(! unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().isEmpty());
		assertEquals(1, unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().size());
		assertTrue(unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().contains(unaPelicula));
		verify(unUsuarioJorge, times(1)).agregarPeliculaDeInteres(unaPelicula);
	}
	
	@Test
	public void dadaUnaSuscripcionAUnGeneroGeneralConSubGenerosDeUnUsuarioConUnaPeliculaDeInteresSiSeLePideQueHagaUpdateConUnaPeliculaLaCualTieneGenerosDeLaSuscripcionVaATenerListaDeNuevasPeliculasDeInteresDeTamaño2()
	{
		//Setup
		ArrayList<String> listaDeGenerosDeLaSuscripcion	=	new ArrayList<String>();
		listaDeGenerosDeLaSuscripcion.add("Accion");
		listaDeGenerosDeLaSuscripcion.add("Guerra");
		listaDeGenerosDeLaSuscripcion.add("2da Guerra Mundial");
		listaDeGenerosDeLaSuscripcion.add("Comedia");
		ArrayList<String> listaDeGenerosDeUnaPelicula	=	new ArrayList<String>();
		listaDeGenerosDeUnaPelicula.add("Comedia");
		when(unaPelicula.listaDeGeneros()).thenReturn(listaDeGenerosDeUnaPelicula);
		when(unGeneroDeAccion.listarGeneros()).thenReturn(listaDeGenerosDeLaSuscripcion);
		
		//Exercise
		unUsuarioJorge.agregarPeliculaDeInteres(unaPelicula2);
		unaSuscripcion.update(unaMoovies, unaPelicula);
		
		//Test
		assertEquals(2, unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().size());
		assertTrue(unaSuscripcion.suscriptor().nuevasPeliculasDeInteres().contains(unaPelicula));
		verify(unUsuarioJorge, times(1)).agregarPeliculaDeInteres(unaPelicula);
	}
}
