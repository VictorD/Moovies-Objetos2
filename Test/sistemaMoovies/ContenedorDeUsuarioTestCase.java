package sistemaMoovies;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ContenedorDeUsuarioTestCase 
{
	@Mock Usuario usuarioPrueba;
	ContenedorDeUsuario unContenedor;

	@Before
	public void setUp() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		unContenedor	= new ContenedorDeUsuario(1 , usuarioPrueba);
	}

	@Test
	public void dadoUnContenedorDeUsuarioSiSeLePideSuUsuarioYSuIdLoDevuelve() 
	{
		
		assertEquals(1, unContenedor.id());
		assertEquals(usuarioPrueba, unContenedor.usuario());
	}

}
