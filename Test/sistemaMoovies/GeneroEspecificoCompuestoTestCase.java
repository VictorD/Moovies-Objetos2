package sistemaMoovies;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class GeneroEspecificoCompuestoTestCase 
{
	GeneroEspecificoCompuesto unGeneroEspecificoCompuesto;
	
	@Before
	public void setUp()
	{
		unGeneroEspecificoCompuesto	= new GeneroEspecificoCompuesto("Terror", new GeneroEspecificoSimple("Fantasmas"));
	}

	@Test
	public void dadoUnGeneroEspecificoCompuestoSiLePidoListarGenerosDevuelveUnaListaDeTamañoDos() 
	{
		//Setup
		ArrayList<String> listaDeGeneros;
		
		//Exercise
		listaDeGeneros	= unGeneroEspecificoCompuesto.listarGeneros();
		
		//Test
		assertEquals(2 , listaDeGeneros.size());
	}
	
	@Test
	public void dadoUnGeneroEspecificoCompuestoConUnSubGeneroSiLeAgregoUnNuevoSubGeneroAlListarDeTamaño3() 
	{
		//Setup
		ArrayList<String> listaDeGeneros;
		GeneroEspecificoSimple unSubGeneroDeTerror	= new GeneroEspecificoSimple("Paranormal");
		
		//Exercise
		unGeneroEspecificoCompuesto.agregarSubGenero(unSubGeneroDeTerror);
		listaDeGeneros	= unGeneroEspecificoCompuesto.listarGeneros();
		
		//Test
		assertEquals(3 , listaDeGeneros.size());
		assertTrue(listaDeGeneros.contains("Paranormal"));
	}

}
