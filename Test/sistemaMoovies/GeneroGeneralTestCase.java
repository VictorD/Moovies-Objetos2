package sistemaMoovies;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class GeneroGeneralTestCase 
{
	@Mock GeneroEspecificoSimple unSubGenero;
	GeneroGeneral unGeneroGeneral;
	
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		unGeneroGeneral	= new GeneroGeneral("Accion");
	}

	@Test
	public void dadoUnGeneroGeneralSiLePidoSuNombreLoDevuelve() 
	{
		assertEquals("Accion",unGeneroGeneral.nombre());
	}

	@Test
	public void dadoUnGeneroGeneralSinSubGenerosSiLePidoSusSubGenerosDevuelveUnaListaVacia()
	{
		assertTrue(unGeneroGeneral.subGeneros().isEmpty());
	}
	
	@Test
	public void dadoUnGeneroGeneralCon1SubGeneroSiLePidoSusSubGenerosDevuelveUnaListaDeTamaño1()
	{
		//Exercise
		unGeneroGeneral.agregarSubGenero(unSubGenero);
		
		//Test
		assertEquals(1, unGeneroGeneral.subGeneros().size());
	}

	@Test
	public void dadoUnGeneroGeneralCon1SubGeneroSiLePidoQueListeLosGenerosDevuelveUnaListaDeTamaño2()
	{
		//Setup
		ArrayList<String> listaDeGeneros;
		ArrayList<String> listaDeGeneroEspecifico	= new ArrayList<String>();
		listaDeGeneroEspecifico.add("Policial");
		when(unSubGenero.listarGeneros()).thenReturn(listaDeGeneroEspecifico);
		
		//Exercise
		unGeneroGeneral.agregarSubGenero(unSubGenero);
		listaDeGeneros	= unGeneroGeneral.listarGeneros();
		
		//Test
		assertEquals(2, listaDeGeneros.size());
		assertTrue(listaDeGeneros.contains("Accion"));
		assertTrue(listaDeGeneros.contains("Policial"));
	}
}
