package sistemaMoovies;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

/**
 * {@link mooviesTestCase} es una clase de prueba para 
 * corroborar el correcto funcionamiento de {@link Moovies}.
 * 
 * @author Victor Degano
 */
public class mooviesTestCase 
{
	@Spy GeneroGeneral unGeneroGeneralDeAccion					= new GeneroGeneral("Accion");
	@Spy GeneroEspecifico unGeneroEspecificoDeGuerra2DaGuerra	= new GeneroEspecificoSimple("2da Guerra Mundial");
	@Spy GeneroEspecifico unGeneroEspecificoDeGuerra			= new GeneroEspecificoCompuesto("Guerra", unGeneroEspecificoDeGuerra2DaGuerra);
	@Spy Usuario usuarioSuscripto	= new Usuario("","",0,"","");
	@Mock Usuario usuario1;
	@Mock Usuario usuario2;
	@Mock Usuario usuario3;
	@Mock Usuario usuario4;
	@Mock Usuario usuario5;
	@Mock Usuario usuario6;
	@Mock Usuario usuario7;
	@Mock Usuario usuario8;
	@Mock Usuario usuario9;
	@Mock Usuario usuario10;
	@Mock Usuario usuario11;
	@Mock Pelicula pelicula1;
	@Mock Pelicula pelicula2;
	@Mock Pelicula pelicula3;	
	@Mock Pelicula pelicula4;	
	@Mock Pelicula pelicula5;	
	@Mock Pelicula pelicula6;	
	@Mock Pelicula pelicula7;	
	@Mock Pelicula pelicula8;	
	@Mock Pelicula pelicula9;	
	@Mock Pelicula pelicula10;	
	@Mock Pelicula pelicula11;	
	@Mock ArrayList<String> generos;
	@Mock Administrador administrador1;
	Moovies moovies;
	
	@Before
	public void setUp() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		moovies			= new Moovies();
	}

	@Test
	public void unaMooviesCuandoSeCreaNoTienePeliculaNiUuariosNiAdministradores() 
	{
		assertTrue(moovies.usuarios().size() == 0);
		assertTrue(moovies.peliculas().size() == 0);
		assertTrue(moovies.administradores().size() == 0);
	}
	
	@Test
	public void siAUnaMooviesRecienCreadaSeLeAgregaUnaPeliculaUnUsuarioYUnAdminitradorSusListasTienenSize1() 
	{
		/*Setup*/
		//Los Mock en este test son dummy
		
		/*Exercise*/
		moovies.agregarUsuario(usuario1, 0);
		moovies.agregarPelicula(pelicula1, 1);
		moovies.agregarAdministrador(administrador1);
		
		/*Test*/
		assertEquals(1 ,moovies.usuarios().size());
		assertEquals(1 ,moovies.peliculas().size());
		assertEquals(1 ,moovies.administradores().size());
	}

	@Test
	public void siSeBuscaAUnUsuarioQueSiEstaEnMooviesEsteEsDevuelto()
	{
		/*Setup*/
		when(usuario1.nombreCompleto()).thenReturn("Jose Perez");
		
		/*Exercise*/
		moovies.agregarUsuario(usuario1, 1);
		
		/*Test*/
		assertTrue(moovies.buscarUsuario("Jose", "Perez") == usuario1);
	}
	
	@Test(expected=RuntimeException.class)
	public void siSeBuscaAUnUsuarioQueNoEstaEnMooviesLevantaUnaExcepcionAvisando()
	{
		/*Setup*/
		when(usuario1.nombreCompleto()).thenReturn("Jose Maco");
		
		/*Test*/
		assertTrue(moovies.buscarUsuario("Jose", "Maco") == usuario1);
	}

	@Test
	public void siSeBuscaUnaPeliculaQueSiEstaEnElSistemaEstaEsDevuelta()
	{
		/*Setup*/
		when(pelicula1.titulo()).thenReturn("El Amanecer de los Muertos");
		
		/*Exercise*/
		moovies.agregarPelicula(pelicula1, 1);
		
		/*Test*/
		assertTrue(moovies.buscarPelicula("El Amanecer de los Muertos") == pelicula1);	
	}

	@Test(expected=RuntimeException.class)
	public void siSeBuscaUnaPeliculaQueNoEstaEnMoovieLevantaUnaExcepcionAvisando()
	{
		/*Setup*/
		when(pelicula1.titulo()).thenReturn("El Amanecer de los Muertos");
		
		/*Test*/
		assertTrue(moovies.buscarPelicula("Amanecer de los Muertos") == pelicula1);	
	}

	@Test
	public void siSeBuscaUnaPeliculaMedianteSuIMDBQueNoEstaEnElSistemaEstaEsDevuelta()
	{
		/*Setup*/
		when(pelicula1.imdb()).thenReturn("TT435FR");
		
		/*Exercise*/
		moovies.agregarPelicula(pelicula1, 1);
		
		/*Test*/
		assertTrue(moovies.buscarPeliculaPorIMDB("TT435FR") == pelicula1);	
	}

	@Test(expected=RuntimeException.class)
	public void siSeBuscaUnaPeliculaMedianteSuIMDBQueNoEstaEnMoovieLevantaUnaExcepcionAvisando()
	{
		/*Setup*/
		when(pelicula1.imdb()).thenReturn("TT435FR");
		
		/*Test*/
		assertTrue(moovies.buscarPeliculaPorIMDB("TT435FR") == pelicula1);	
	}
	
	@Test
	public void siSeLePideLos10UsuariosMasActivosYNoHayNingunoDevuelveUnaListaVacia()
	{
		//Test
		assertEquals(0, moovies.usuariosMasActivos().size());
	}

	@Test
	public void siSeLePideLos10UsuariosMasActivosYHayUnSoloUsuario_DevuelveUnaListaDeTamaño1()
	{
		//Setup
		when(usuario1.cantidadDeCalificaciones()).thenReturn(1);
		
		//Exercise
		moovies.agregarUsuario(usuario1, 0);
		
		//Test
		assertEquals(1, moovies.usuariosMasActivos().size());
		
	}

	@Test
	public void siSeLePideLos10UsuariosMasActivosYHaySoloDosUsuario_DevuelveUnaListaDeTamaño2DondeElPrimerUsuarioCalificoMasPeliculasQueElSegundo()
	{
		//Setup
		List<Usuario> usuariosMasActivos	= new ArrayList<Usuario>();
		when(usuario1.cantidadDeCalificaciones()).thenReturn(2);
		when(usuario2.cantidadDeCalificaciones()).thenReturn(1);

		//Exercise
		moovies.agregarUsuario(usuario1, 0);
		moovies.agregarUsuario(usuario2, 1);
		usuariosMasActivos	= moovies.usuariosMasActivos();
		
		//Test
		assertEquals(2, usuariosMasActivos.size());
		assertTrue(usuariosMasActivos.get(0).cantidadDeCalificaciones() > usuariosMasActivos.get(1).cantidadDeCalificaciones());
	}
	
	@Test
	public void siSeLePideLos10UsuariosMasActivosYHayOnceUsuario_DevuelveUnaListaDeTamaño10()
	{
		//Setup
		List<Usuario> usuariosMasActivos	= new ArrayList<Usuario>();
		when(usuario1.cantidadDeCalificaciones()).thenReturn(9);
		when(usuario2.cantidadDeCalificaciones()).thenReturn(8);
		when(usuario3.cantidadDeCalificaciones()).thenReturn(7);
		when(usuario4.cantidadDeCalificaciones()).thenReturn(6);
		when(usuario5.cantidadDeCalificaciones()).thenReturn(5);
		when(usuario6.cantidadDeCalificaciones()).thenReturn(4);
		when(usuario7.cantidadDeCalificaciones()).thenReturn(4);
		when(usuario8.cantidadDeCalificaciones()).thenReturn(3);
		when(usuario9.cantidadDeCalificaciones()).thenReturn(2);
		when(usuario10.cantidadDeCalificaciones()).thenReturn(1);
		when(usuario11.cantidadDeCalificaciones()).thenReturn(0);

		//Exercise
		moovies.agregarUsuario(usuario1, 0);
		moovies.agregarUsuario(usuario2, 1);
		moovies.agregarUsuario(usuario3, 2);
		moovies.agregarUsuario(usuario4, 3);
		moovies.agregarUsuario(usuario5, 4);
		moovies.agregarUsuario(usuario6, 5);
		moovies.agregarUsuario(usuario7, 6);
		moovies.agregarUsuario(usuario8, 7);
		moovies.agregarUsuario(usuario9, 8);
		moovies.agregarUsuario(usuario10, 9);
		moovies.agregarUsuario(usuario11, 10);
		
		usuariosMasActivos = moovies.usuariosMasActivos();
		
		//Test
		assertEquals(10, usuariosMasActivos.size());
		assertFalse(usuariosMasActivos.contains(usuario11));
		assertEquals(0 , usuariosMasActivos.indexOf(usuario1));
	}
	
	@Test
	public void siSeLePideLasMejoresPeliculasYNoHayNingunaDevuelveUnaListaVacia()
	{
		//Test
		assertEquals(0, moovies.mejoresPeliculas().size());
	}

	@Test
	public void siSeLePideLasMejoresPeliculasYHayUnaSolaPelicula_DevuelveUnaListaDeTamaño1()
	{
		//Setup
		when(pelicula1.promedio()).thenReturn(1);
		
		//Exercise
		moovies.agregarPelicula(pelicula1, 1);
		
		//Test
		assertEquals(1, moovies.mejoresPeliculas().size());
	}

	@Test
	public void siSeLePideLasMejoresPeliculasYHaySoloDosPeliculas_DevuelveUnaListaDeTamaño2DondeLaPrimerPeliculaTieneMejorPromedioQueLaSegunda()
	{
		//Setup
		List<Pelicula> mejoresPelis	= new ArrayList<Pelicula>();
		when(pelicula1.promedio()).thenReturn(5);
		when(pelicula2.promedio()).thenReturn(3);
		
		//Exercise
		moovies.agregarPelicula(pelicula1, 1);
		moovies.agregarPelicula(pelicula2, 2);
		mejoresPelis	= moovies.mejoresPeliculas();
		
		//Test
		assertEquals(2, mejoresPelis.size());
		assertTrue(mejoresPelis.get(0).promedio() > mejoresPelis.get(1).promedio());
	}
	
	@Test
	public void siSeLePideLasMejoresPeliculasYHayOncePeliculas_DevuelveUnaListaDeTamaño10()
	{
		//Setup
		List<Pelicula> mejoresPelis	= new ArrayList<Pelicula>();
		when(pelicula1.promedio()).thenReturn(10);
		when(pelicula2.promedio()).thenReturn(9);
		when(pelicula3.promedio()).thenReturn(8);
		when(pelicula4.promedio()).thenReturn(7);
		when(pelicula5.promedio()).thenReturn(6);
		when(pelicula6.promedio()).thenReturn(5);
		when(pelicula7.promedio()).thenReturn(4);
		when(pelicula8.promedio()).thenReturn(3);
		when(pelicula9.promedio()).thenReturn(2);
		when(pelicula10.promedio()).thenReturn(1);
		when(pelicula11.promedio()).thenReturn(0);
		
		//Exercise
		moovies.agregarPelicula(pelicula1, 1);
		moovies.agregarPelicula(pelicula2, 2);
		moovies.agregarPelicula(pelicula3, 3);
		moovies.agregarPelicula(pelicula4, 4);
		moovies.agregarPelicula(pelicula5, 5);
		moovies.agregarPelicula(pelicula6, 6);
		moovies.agregarPelicula(pelicula7, 7);
		moovies.agregarPelicula(pelicula8, 8);
		moovies.agregarPelicula(pelicula9, 9);
		moovies.agregarPelicula(pelicula10, 10);
		moovies.agregarPelicula(pelicula11, 11);
		mejoresPelis	= moovies.mejoresPeliculas();

		//Test
		assertEquals(10, mejoresPelis.size());
		assertFalse(mejoresPelis.contains(pelicula11));
	}
	
	@Test
	public void dadaUnaMooviesConUnSoloUsuarioSiSeLePreguntaSiHayAmistadesRespondeFalse()
	{
		//Setup
		when(usuario1.tieneAmigos()).thenReturn(false);
		
		//Exercise
		moovies.agregarUsuario(usuario1, 1);

		//Test
		assertTrue(1 == moovies.usuarios().size());
		assertFalse(moovies.hayAmigos());
	}
	
	@Test
	public void dadaUnaMooviesConDosUsuariosYSonAmigosSiSeLePreguntaSiHayAmistadesRespondeTrue()
	{
		//Setup
		when(usuario1.tieneAmigos()).thenReturn(true);
		when(usuario2.tieneAmigos()).thenReturn(true);
		
		//Exercise
		moovies.agregarUsuario(usuario1, 1);
		moovies.agregarUsuario(usuario2, 2);

		//Test
		assertTrue(2 == moovies.usuarios().size());
		assertTrue(moovies.hayAmigos());
	}
	
	@Test
	public void siAUnaMoovieConUnaPelicula1SeLePreguntaSiEstaLaPelicula1RespondeTrue()
	{
		//Exercise
		moovies.agregarPelicula(pelicula1, 1);
		
		//Test
		assertTrue(moovies.estaLaPelicula(pelicula1));
	}
	
	@Test
	public void siAUnaMoovieQueNoTieneLaPelicula1SeLePreguntaSiEstaLaPelicula1RespondeFalse()
	{		
		//Test
		assertFalse(moovies.estaLaPelicula(pelicula1));
	}
	
	@Test
	public void siSeBuscaUnaPeliculaPorIDQueSiEstaEnElSistemaEstaEsDevuelta()
	{
		/*Exercise*/
		moovies.agregarPelicula(pelicula1, 1);
		
		/*Test*/
		assertTrue(moovies.buscarPeliculaPorID(1) == pelicula1);	
	}

	@Test(expected=RuntimeException.class)
	public void siSeBuscaUnaPeliculaPorIDQueNoEstaEnMoovieLevantaUnaExcepcionAvisando()
	{
		/*Test*/
		assertTrue(moovies.buscarPeliculaPorID(1) == pelicula1);	
	}

	@Test
	public void dadoUnSistemaMooviesSiSeAgregaUnaPeliculaDeUnGeneroDelCualUnUsuarioEstaSuscriptoYNoTienePeliculasDeInteres_ElUsuarioVaATenerUnaListaDePeliculasDeInteresDeTamaño1()
	{
		//Setup
		ArrayList<String> unaListaDeGenerosDePelicula1	= new ArrayList<String>();
		unaListaDeGenerosDePelicula1.add("Accion");
		unaListaDeGenerosDePelicula1.add("Guerra");
		unaListaDeGenerosDePelicula1.add("2da Guerra Mundial");
		ArrayList<Genero> unaListaDeGeneros				= new ArrayList<Genero>();
		unaListaDeGeneros.add(unGeneroEspecificoDeGuerra2DaGuerra);
		unGeneroGeneralDeAccion.agregarSubGenero(unGeneroEspecificoDeGuerra);
		usuarioSuscripto.nivelDePrivilegios(new NivelUsuario(moovies));
		when(pelicula1.listaDeGeneros()).thenReturn(unaListaDeGenerosDePelicula1);
		
		//Exercise
		usuarioSuscripto.suscribirseA(unaListaDeGeneros);
		moovies.agregarPelicula(pelicula1, 156);
		
		//Test
		assertEquals(1, usuarioSuscripto.nuevasPeliculasDeInteres().size());
		assertTrue(usuarioSuscripto.nuevasPeliculasDeInteres().contains(pelicula1));
	}
	
}