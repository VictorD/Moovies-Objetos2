package sistemaMoovies;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GeneroEspecificoSimpleTestCase 
{
	GeneroEspecifico unGeneroEspecificoSimple;
	
	@Before
	public void setUp() throws Exception 
	{
		unGeneroEspecificoSimple	= new GeneroEspecificoSimple("Vampiros");
	}

	@Test
	public void dadoUnGeneroEspecificoSimpleSiLePidoSuNombreLoDevuelve() 
	{
		assertEquals("Vampiros", unGeneroEspecificoSimple.nombre());
	}

	@Test
	public void dadoUnGeneroEspecificoSimpleSiLePidoListarLosGenerosDevuelveUnaListaConSoloSuNombre() 
	{
		assertEquals(1, unGeneroEspecificoSimple.listarGeneros().size());
		assertEquals("Vampiros", unGeneroEspecificoSimple.listarGeneros().get(0));
	}
	
	@Test
	public void dadoUnGeneroEspecificoSimpleSiLeAgregoUnSubGeneroDevuelveUnGeneroEspecificoCompuesto()
	{
		//Setup
		GeneroEspecificoSimple	unGeneroSimple	= new GeneroEspecificoSimple("Vampiros vs Lobos");
		GeneroEspecifico unGeneroCompuesto;
		
		//Exercise
		unGeneroCompuesto		= unGeneroEspecificoSimple.agregarSubGenero(unGeneroSimple);
		
		//Test
		assertEquals(GeneroEspecificoCompuesto.class, unGeneroCompuesto.getClass());
		assertEquals("Vampiros", unGeneroCompuesto.nombre());
		assertTrue(unGeneroCompuesto.listarGeneros().contains("Vampiros vs Lobos"));
		
	}
}
