package sistemaMoovies;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import java.util.Comparator;


public class OrdenarUsuariosPorCantidadDeCalificacionesTestCase 
{
	Comparator<Usuario> comparador;
	@Mock Usuario usuarioA;
	@Mock Usuario usuarioB;
	
	@Before
	public void setUp() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		comparador	= new OrdenarUsuariosPorCantidadDeCalificaciones();
	}

	@Test
	public void dadoElUsuarioACon2CalificacionesYElUsuarioBCon0CalificacionesSiSeComparanRetorna1Negativo() 
	{
		//Setup
		when(usuarioA.cantidadDeCalificaciones()).thenReturn(2);
		when(usuarioB.cantidadDeCalificaciones()).thenReturn(0);
		
		//Test
		assertEquals(-1,comparador.compare(usuarioA, usuarioB));
	}
	
	@Test
	public void dadoElUsuarioACon2CalificacionesYElUsuarioBCon2CalificacionesSiSeComparanRetorna0() 
	{
		//Setup
		when(usuarioA.cantidadDeCalificaciones()).thenReturn(2);
		when(usuarioB.cantidadDeCalificaciones()).thenReturn(2);

		//Test
		assertEquals(0, comparador.compare(usuarioA, usuarioB));
	}

	@Test
	public void dadoElUsuarioACon0CalificacionesYElUsuarioBCon2CalificacionesSiSeComparanRetorna1() 
	{
		//Setup
		when(usuarioA.cantidadDeCalificaciones()).thenReturn(0);
		when(usuarioB.cantidadDeCalificaciones()).thenReturn(2);

		//Test
		assertEquals(1, comparador.compare(usuarioA, usuarioB));
	}
}
