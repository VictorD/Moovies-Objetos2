package sistemaMoovies;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GeneroEspecificoTestCase 
{
	GeneroEspecificoSimple unGeneroEspecificoSimpleAccion;
	GeneroEspecificoSimple unGeneroEspecificoSimplePolicial;
	
	@Before
	public void setUp() 
	{
		unGeneroEspecificoSimpleAccion	= new GeneroEspecificoSimple("Accion");
		unGeneroEspecificoSimplePolicial= new GeneroEspecificoSimple("Policial");
	}

	@Test
	public void dadoUnGeneroEspecificoSiHagoUnComposeEntreDosGenerosSimplesDevuelveUnGeneroEspecificoCompuesto() 
	{
		//Setup
		GeneroEspecifico unGeneroAccionConSubGeneroPolicial;
		
		//Exercise
		unGeneroAccionConSubGeneroPolicial	= unGeneroEspecificoSimpleAccion.compose(unGeneroEspecificoSimplePolicial);
		
		//Test
		assertEquals(2 , unGeneroAccionConSubGeneroPolicial.listarGeneros().size());
		assertEquals("Accion" , unGeneroAccionConSubGeneroPolicial.nombre());
		assertTrue(unGeneroAccionConSubGeneroPolicial.listarGeneros().contains("Policial"));
	}

}
