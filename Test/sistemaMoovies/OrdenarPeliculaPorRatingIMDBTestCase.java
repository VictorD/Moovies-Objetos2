package sistemaMoovies;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OrdenarPeliculaPorRatingIMDBTestCase {

	Comparator<Pelicula> comparador;
	@Mock Pelicula pelicula1;
	@Mock Pelicula pelicula2;
	
	@Before
	public void setUp() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		comparador	= new OrdenarPeliculaPorRatingIMDB();
	}

	@Test
	public void Dadas_Dos_peliculas_La_Primera_Tiene_Rating_IMDB_Mayor_A_La_Segunda_Al_Comparar_Sus_Ratings_Retorna_Negativo_Porque_La_Primera_Pelicula_Tiene_Un_Rating_Mayor()
	{
		when(pelicula1.ratingIMDB()).thenReturn(7);
		when(pelicula2.ratingIMDB()).thenReturn(3);
		
		assertEquals(-1, comparador.compare(pelicula1, pelicula2));
	}
	
	@Test
	public void Dadas_Dos_peliculas_La_Primera_Tiene_Rating_IMDB_Menor_A_La_Segunda_Al_Comparar_Sus_Ratings_Retorna_Positivo_Porque_La_Segunda_Pelicula_Tiene_Un_Rating_Mayor()
	{
		when(pelicula1.ratingIMDB()).thenReturn(1);
		when(pelicula2.ratingIMDB()).thenReturn(5);
		
		assertEquals(1, comparador.compare(pelicula1, pelicula2));
	}
	
	@Test
	public void Dadas_Dos_Peliculas_Con_Igual_Rating_IMDB_Al_Compararlas_Obtento_0_Por_La_Igualdad_de_La_Comparacion()
	{
		when(pelicula1.ratingIMDB()).thenReturn(4);
		when(pelicula2.ratingIMDB()).thenReturn(4);
		
		assertEquals(0, comparador.compare(pelicula1, pelicula2));
	}
}
