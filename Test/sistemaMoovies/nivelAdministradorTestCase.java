package sistemaMoovies;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import csvFileReader.Contactos;

import static org.mockito.Mockito.*;

public class nivelAdministradorTestCase {

	NivelAdministrador nivelAdminPrueba; 
	ArrayList<ContenedorDePelicula> listaDePelisAPasar;
	ArrayList<ContenedorDeUsuario> listaDeUsuariosAPasar;
	List<Rating> listaDeRatings;
	Moovies mooviePrueba;	
	List<Contactos> unaListaDeAmigos;
	
	@Mock Pelicula peliUnNombre1;
	@Mock Pelicula peliUnNombre2;
	@Mock Pelicula peliUnNombre3;
	@Mock Usuario userVictor;
	@Mock Usuario userMauricio;
	@Mock Usuario userNadia;
	@Mock ContenedorDePelicula contenedorPeli1;
	@Mock ContenedorDePelicula contenedorPeli2;
	@Mock ContenedorDePelicula contenedorPeli3;
	@Mock ContenedorDeUsuario contenedorUser1;
	@Mock ContenedorDeUsuario contenedorUser2;
	@Mock ContenedorDeUsuario contenedorUser3;
	@Mock Rating rating1;
	@Mock Rating rating2;
	@Mock Contactos nuevaAmistad;
	
	@Before
	public void setUp() throws Exception 
	{
		MockitoAnnotations.initMocks(this);
		listaDePelisAPasar		= new ArrayList<ContenedorDePelicula>();
		listaDeUsuariosAPasar	= new ArrayList<ContenedorDeUsuario>();
		listaDeRatings			= new ArrayList<Rating>();
		unaListaDeAmigos		= new ArrayList<Contactos>();
		mooviePrueba			= new Moovies();
		nivelAdminPrueba		= new NivelAdministrador(mooviePrueba);
	}

	@Test
	public void siSeQuiereImportar3PeliculasEnUnaMooviesSinPeliculas_MooviesVaATener3PeliculasEnSuBaseDeDatos()
	{
		//Setup
		when(contenedorPeli1.id()).thenReturn(1);
		when(contenedorPeli1.pelicula()).thenReturn(peliUnNombre1);
		when(contenedorPeli2.id()).thenReturn(2);
		when(contenedorPeli2.pelicula()).thenReturn(peliUnNombre2);
		when(contenedorPeli3.id()).thenReturn(3);
		when(contenedorPeli3.pelicula()).thenReturn(peliUnNombre3);
		
		
		//Exercise
		listaDePelisAPasar.add(contenedorPeli1);
		listaDePelisAPasar.add(contenedorPeli2);
		listaDePelisAPasar.add(contenedorPeli3);
		nivelAdminPrueba.cargarPeliculasImportadas(listaDePelisAPasar);
		
		//Test
		assertEquals(3, mooviePrueba.peliculas().size());
	}

	@Test
	public void siSeQuiereImportarLaMismaPeliculaQueEstaEnMooviesNoLoAgrega()
	{
		//Setup
		when(contenedorPeli1.id()).thenReturn(1);
		when(contenedorPeli1.pelicula()).thenReturn(peliUnNombre1);

		//Exercise
		mooviePrueba.agregarPelicula(peliUnNombre1, 1);
		listaDePelisAPasar.add(contenedorPeli1);
		nivelAdminPrueba.cargarPeliculasImportadas(listaDePelisAPasar);
		
		//Test
		assertEquals(1, mooviePrueba.peliculas().size());
	}

	@Test
	public void siSeQuiereImportarUnaListaDondeDeDosPeliculasDondeUnaYaEstaEnMooviesSoloAgregaLaQueNoEstaRepetida()
	{
		//Setup
		when(contenedorPeli1.id()).thenReturn(1);
		when(contenedorPeli1.pelicula()).thenReturn(peliUnNombre1);
		when(contenedorPeli2.id()).thenReturn(2);
		when(contenedorPeli2.pelicula()).thenReturn(peliUnNombre2);
		
		//Exercise
		mooviePrueba.agregarPelicula(peliUnNombre1, 1);
		listaDePelisAPasar.add(contenedorPeli1);
		listaDePelisAPasar.add(contenedorPeli2);
		nivelAdminPrueba.cargarPeliculasImportadas(listaDePelisAPasar);
		
		//Test
		assertEquals(2, mooviePrueba.peliculas().size());
	}
	
	@Test
	public void siSeQuiereImportar3UsuariosQueNoEstanEnMoovies_MooviesVaATener3UsuariosEnSuBaseDeDatos()
	{
		//Setup
		when(contenedorUser1.id()).thenReturn(1);
		when(contenedorUser1.usuario()).thenReturn(userVictor);
		when(contenedorUser2.id()).thenReturn(2);
		when(contenedorUser2.usuario()).thenReturn(userMauricio);
		when(contenedorUser3.id()).thenReturn(3);
		when(contenedorUser3.usuario()).thenReturn(userNadia);
		
		//Exercise
		listaDeUsuariosAPasar.add(contenedorUser1);
		listaDeUsuariosAPasar.add(contenedorUser2);
		listaDeUsuariosAPasar.add(contenedorUser3);
		nivelAdminPrueba.cargarUsuariosImportados(listaDeUsuariosAPasar);
		
		//Test
		assertEquals(3, mooviePrueba.usuarios().size());
	}
	
	@Test
	public void siSeQuiereImportarElMismoUsuarioQueYaEstaEnElSistema_NoLoAgrega()
	{
		//Setup
		when(contenedorUser1.id()).thenReturn(1);
		when(contenedorUser1.usuario()).thenReturn(userVictor);
				
		//Exercise
		mooviePrueba.agregarUsuario(userVictor, 1);
		listaDeUsuariosAPasar.add(contenedorUser1);
		nivelAdminPrueba.cargarUsuariosImportados(listaDeUsuariosAPasar);
		
		//Test
		assertTrue(mooviePrueba.usuarios().size() == 1);
	}
	
	@Test
	public void siSeQuiereImportar2UsuariosDondeUnoYaEstaEnElSistema_SoloAgregaAlQueNoEsta()
	{
		//Setup
		when(contenedorUser1.id()).thenReturn(1);
		when(contenedorUser1.usuario()).thenReturn(userVictor);
		when(contenedorUser2.id()).thenReturn(2);
		when(contenedorUser2.usuario()).thenReturn(userMauricio);		
				
		//Exercise
		mooviePrueba.agregarUsuario(userVictor, 1);
		mooviePrueba.agregarUsuario(userMauricio, 2);
		listaDeUsuariosAPasar.add(contenedorUser1);
		nivelAdminPrueba.cargarUsuariosImportados(listaDeUsuariosAPasar);
		
		//Test
		assertEquals(2, mooviePrueba.usuarios().size());
	}

	@Test
	public void siSeQuiereImportarRatingsDondeNoHayPeliculasNiUsuarios_NoImportaNada()
	{
		//Setup
		when(rating1.idUsuario()).thenReturn(1);
		when(rating1.peliculaId()).thenReturn(3);
		
		//Exercise
		listaDeRatings.add(rating1);
		nivelAdminPrueba.cargarRatingsImportados(listaDeRatings);
		
		//Test
		assertFalse(mooviePrueba.hayRatings());
	}
	
	@Test
	public void siSeQuiereImportarRatingsYNoHayPeliculasPeroSiUsuariosNoImportaNada()
	{
		//Setup
		when(rating1.idUsuario()).thenReturn(0);
		when(rating1.peliculaId()).thenReturn(3);
		
		//Exercise
		listaDeRatings.add(rating1);
		mooviePrueba.agregarUsuario(userVictor, 0);
		nivelAdminPrueba.cargarRatingsImportados(listaDeRatings);
		
		//Test
		assertFalse(mooviePrueba.hayRatings());
	}
	
	@Test
	public void siSeQuiereImportarRatingsYNoHayUsuariosPeroSiPeliculasNoImportaNada()
	{
		//Setup
		when(rating1.idUsuario()).thenReturn(0);
		when(rating1.peliculaId()).thenReturn(3);
		
		//Exercise
		listaDeRatings.add(rating1);
		mooviePrueba.agregarPelicula(peliUnNombre1, 3);
		nivelAdminPrueba.cargarRatingsImportados(listaDeRatings);
		
		//Test
		assertFalse(mooviePrueba.hayRatings());
	}
	
	@Test
	public void siSeQuiereImportarUnRatingsDeLaUnicaPeliculaCalificadaPorElUnicoUsuarioDeMooviesLoHace()
	{
		//Setup
		Pelicula peliUnNombre4 	= new Pelicula("Ranger", LocalDate.now(), "TT34T", new ArrayList<>());
		Usuario userVictor1		= new Usuario("Victor", "Degano", 0, "", " ");
		when(rating1.idUsuario()).thenReturn(1);
		when(rating1.peliculaId()).thenReturn(3);
		
		//Exercise
		mooviePrueba.agregarUsuario(userVictor1, 1);
		mooviePrueba.agregarPelicula(peliUnNombre4, 3);
		listaDeRatings.add(rating1);
		nivelAdminPrueba.cargarRatingsImportados(listaDeRatings);

		//Test
		assertEquals(true, mooviePrueba.hayRatings());
		
	}

	@Test
	public void siSeQuiereImportarAmigosAUnaMooviesSinUsuariosNoHaceNada()
	{
		//Setup
		when(nuevaAmistad.usuarioID()).thenReturn(1);
		when(nuevaAmistad.amigoID()).thenReturn(2);
		
		//Exercise
		unaListaDeAmigos.add(nuevaAmistad);
		nivelAdminPrueba.cargarAmigosImportados(unaListaDeAmigos);
		
		//Test
		assertFalse(mooviePrueba.hayAmigos());
	}
	
	@Test
	public void siSeImportaUnaListaDeUnaUnicaRelacionDeAmistadQueSonLosUnicosDosUsuariosDeMooviesLoImporta()
	{
		//Setup
		when(nuevaAmistad.usuarioID()).thenReturn(1);
		when(nuevaAmistad.amigoID()).thenReturn(2);
		userVictor	= new Usuario("Victor", "Degano", 21, "", "");
		userMauricio= new Usuario("Mauricio", "Salinas", 22, "", "");
		
		//Exercise
		unaListaDeAmigos.add(nuevaAmistad);
		mooviePrueba.agregarUsuario(userVictor, 1);
		mooviePrueba.agregarUsuario(userMauricio, 2);
		nivelAdminPrueba.cargarAmigosImportados(unaListaDeAmigos);
		
		//Test
		assertTrue(mooviePrueba.hayAmigos());
	}
	
	@Test
	public void siSeQuiereImportarRatingsIMDBDeUnaPeliculaVaATenerRatingIMDB3()
	{
		//Setup
		Pelicula peliUnNombre4 	= new Pelicula("Ranger", LocalDate.now(), "TT34T", new ArrayList<>());
//		Usuario userVictor1		= new Usuario("Victor", "Degano", 0, "", " ");
//		when(rating1.idUsuario()).thenReturn(1);
		when(rating1.peliculaId()).thenReturn(2);
		when(rating1.calificacion()).thenReturn(3);
//		when(rating2.idUsuario()).thenReturn(1);
		when(rating2.peliculaId()).thenReturn(2);
		when(rating2.calificacion()).thenReturn(3);
		
		//Exercise
		mooviePrueba.agregarPelicula(peliUnNombre4, 2);
		listaDeRatings.add(rating1);
		listaDeRatings.add(rating2);
		nivelAdminPrueba.cargarRatingsIMDBImportados(listaDeRatings);

		//Test
		assertEquals(3, peliUnNombre4.ratingIMDB());
		
	}

}
