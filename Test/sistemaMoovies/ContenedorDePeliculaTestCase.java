package sistemaMoovies;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ContenedorDePeliculaTestCase 
{
	@Mock Pelicula peliculaPrueba;
	ContenedorDePelicula unContenedor;

	@Before
	public void setUp() 
	{
		MockitoAnnotations.initMocks(this);
		unContenedor	= new ContenedorDePelicula(1 , peliculaPrueba);
	}

	@Test
	public void dadoUnContenedorDePeliculaSiSeLePideSuPeliculaoYSuIdLoDevuelve() 
	{
		
		assertEquals(1, unContenedor.id());
		assertEquals(peliculaPrueba, unContenedor.pelicula());
	}
}
