package sistemaMoovies;

import java.time.LocalDate;
import java.util.ArrayList;

public class Pelicula
{
	/*Atributos*/
	private String titulo;
	private String imdb;
	private ArrayList<Rating> ratings;
	private ArrayList<Genero> generos;
	private LocalDate fechaDeEstreno;
	private Integer ratingIMDB;
	

	public Pelicula(String unTitulo, LocalDate fechaDeEstreno, String unImdb, ArrayList<Genero> generosDePelicula) 
	{
		this.titulo   	    = unTitulo;
		this.fechaDeEstreno = fechaDeEstreno;
		this.imdb	        = unImdb;
		this.ratings	    = new ArrayList<Rating>();
		this.generos        = generosDePelicula;
		this.ratingIMDB     = 0;
	}

	/*Getters & Setters*/
	private String getTitulo()
	{
		return this.titulo;
	}

	private String getIMDB()
	{
		return this.imdb;
	}
	
	private ArrayList<Rating> getRatings()
	{
		return this.ratings;
	}
	
	private ArrayList<Genero> getGeneros() 
	{
		return this.generos;
	}

	private LocalDate getFechaDeEstreno() 
	{
		return this.fechaDeEstreno;
	}
	
	private void setRatingIMDB(Integer puntaje) 
	{
		this.ratingIMDB = puntaje;
	}
	
	private int getRatingIMDB() 
	{
		return this.ratingIMDB;
	}
	
	/*Metodos*/

	public String titulo()
	{
		return this.getTitulo();
	}
	
	
	public String imdb()
	{
		return this.getIMDB();
	}
	
	private boolean estaRating(Rating unRating)
	{
		Boolean estaRating = new Boolean(false);
		
		for(Rating rating : this.ratings)
		{
			estaRating = estaRating || (rating.idUsuario() == unRating.idUsuario());
		}
		return estaRating;
	}
	
	

	public void guardarRating(Rating ratingAGuardar)
	{
		if (! this.estaRating(ratingAGuardar))
		{
			this.getRatings().add(ratingAGuardar);
		}
	}
	
	public int promedio()
	{
		int contador	= 0;
		int acumulador	= 0;
		ArrayList<Rating> misCalificaciones	= this.getRatings();
		int tamanioCalificaciones	= misCalificaciones.size();
		if (! misCalificaciones.isEmpty())
		{
		while(contador < tamanioCalificaciones)
		{
			acumulador	= acumulador + misCalificaciones.get(contador).calificacion();
			contador++;
		}
		return acumulador / contador;
		}
		else
		{
			return 0;
		}
	}
	
	
	public ArrayList<Rating> ratings()
	{
		return this.getRatings();
	}


	public ArrayList<Genero> generos() {
		return this.getGeneros();
	}

	public LocalDate fechaDeEstreno()
	{
		return this.getFechaDeEstreno();
	}


	public ArrayList<String> informacionDePelicula() 
	{
		ArrayList<String> datosPelicula	= new ArrayList<String>();
		
		datosPelicula.add("Nombre: "+ this.titulo());
		datosPelicula.add("Fecha de estreno: "+ this.fechaDeEstreno());
		datosPelicula.add("Generos: " +this.listaDeGeneros());
		
		return datosPelicula;
		
	}


	public ArrayList<String> listaDeGeneros() 
	{
		ArrayList<String> todosLosGeneros	= new ArrayList<String>();
		for (Genero unGenero : this.generos())
		{
			todosLosGeneros.addAll(unGenero.listarGeneros());
		}
		return todosLosGeneros;
	}

	/**
	 * 
	 * @return
	 * @author Victor Degano
	 */
	public boolean hayRatings() 
	{
		return this.getRatings().size() > 0;
	}
	
	public void actualizarRatingIMDB(Integer puntaje) 
	{
		this.setRatingIMDB(puntaje);
	}

	public int ratingIMDB() 
	{
		return getRatingIMDB();
	}

}	
	
	