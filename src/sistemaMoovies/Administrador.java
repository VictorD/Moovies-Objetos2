package sistemaMoovies;

import java.util.List;

import csvFileReader.CSVFileReader;
import csvFileReader.Contactos;
import csvFileReader.ContactosFileReader;
import csvFileReader.PeliculaFileReader;
import csvFileReader.RatingFileReader;
import csvFileReader.UsuarioFileReader;

/**
 * Clase que representa a un administrador de un sistema {@linkplain Moovies}
 * @author Victor Degano
 */
public class Administrador extends Persona
{
	//Atributos
	private NivelAdministrador nivelDePrivilegios;


	//Constructores
	public Administrador(String unNombre,String unApellido, int unaEdad, String unaOcupacion, String unCodigoPostal)
	{
		super(unNombre, unApellido, unaEdad, unaOcupacion, unCodigoPostal);
		this.nivelDePrivilegios	= new NivelAdministrador(new Moovies());
	}
	
	//Getters && Setters
	private NivelAdministrador getPrivilegios()
	{
		return this.nivelDePrivilegios;
	}
	
	private void setNivelDePrivilegios(NivelAdministrador unNivelDePrivilegios)
	{
		this.nivelDePrivilegios	= unNivelDePrivilegios;
	}
	
	//Metodos
	/**
	 *Agrega el nivel de privilegios del administrador ante el sistema moovies. 	
	 * @param unNivelDePrivilegios	-	el {@linkplain NivelAdministrador} que 
	 * se le desea poner al Administrador.
	 * @author Victor Degano
	 */
	public void nivelDePrivilegios(NivelAdministrador unNivelDePrivilegios)
	{
		this.setNivelDePrivilegios(unNivelDePrivilegios);
	}
	
	/**
	 * Importa un archivo .csv para actualizar la base de datos de peliculas de moovie
	 * @param String - Es la direccion en la cual esta alojado el .csv de peliculas
	 * @author Victor Degano
	 */
	public void importarPeliculas(String unaDireccion)
	{
		CSVFileReader<ContenedorDePelicula> csvPeliculas= new PeliculaFileReader(unaDireccion);
		List<ContenedorDePelicula>	peliculasAImportar	= csvPeliculas.readFile();
		this.getPrivilegios().cargarPeliculasImportadas(peliculasAImportar);
	}

	/**
	 * Importa un archivo .csv para actualizar la base de datos de usuarios de moovie
	 * @param String - Es la direccion en la cual esta alojado el .csv de usuario
	 * @author Victor Degano
	 */
	public void importarUsuarios(String unaDireccion)
	{
		CSVFileReader<ContenedorDeUsuario> csvUsuario= new UsuarioFileReader(unaDireccion);
		List<ContenedorDeUsuario>	usuarioAImportar	= csvUsuario.readFile();
		this.getPrivilegios().cargarUsuariosImportados(usuarioAImportar);
	}
	
	/**
	 * Importa un archivo .csv para cargar las calificaciones de los usuarios y peliculas a moovies
	 * @param String - Es la direccion en la cual esta alojado el .csv de las calificaciones
	 * @author Victor Degano
	 */
	public void importarRatings(String unaDireccion)
	{
		CSVFileReader<Rating> csvRating= new RatingFileReader(unaDireccion);
		List<Rating>	ratingAImportar	= csvRating.readFile();
		this.getPrivilegios().cargarRatingsImportados(ratingAImportar);
	}

	/**
	 * Importa un archivo .csv para actualizar las amistades de los usuarios de moovies
	 * @param String - Es la direccion en la cual esta alojado el .csv de las amistades
	 * @author Victor Degano
	 */
	public void importarAmigos(String unaDireccion)
	{
		CSVFileReader<Contactos> csvAmigos= new ContactosFileReader(unaDireccion);
		List<Contactos>	amigosAImportar	= csvAmigos.readFile();
		this.getPrivilegios().cargarAmigosImportados(amigosAImportar);
	}
	
}