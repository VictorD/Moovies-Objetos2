package sistemaMoovies;

import java.util.ArrayList;
import java.util.Collections;


public class PeliculasCalificadasPor2OMasAmigosCon3OMasOrdenadasPorRatingIMDB implements MetodoDeRecomendacionDePeliculas {

	//Cuando el usuario seleccione este metodo de recomendacion, se recomendaran aquellas peliculas que
	//hayan sido calificadas por 2 o mas amigos con un puntaje de 3 o mas y se entregaran ordenadas
	//por rating IMDB.
	
	@Override
	public ArrayList<Pelicula> filtrarRecomendadas(Usuario usuario, ArrayList<Usuario> amigos) 
	{
		
		ArrayList<Pelicula> peliculasDeUsuarios = new ArrayList<Pelicula>();		
		ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
		
		for(Usuario amigo : amigos)
		{
			peliculasDeUsuarios.addAll(amigo.peliculasCalificadasConPuntajeMinimo(3));
		}
		
		for(Pelicula pelicula : peliculasDeUsuarios)
		{
			if(Collections.frequency(peliculasDeUsuarios, pelicula ) >= 2 && !peliculas.contains(pelicula))
			{
				peliculas.add(pelicula);
			}
		}
		
		peliculas.sort(new OrdenarPeliculaPorRatingIMDB());
		return peliculas;
	}

}
