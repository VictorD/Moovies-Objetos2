package sistemaMoovies;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

/**
 * Esta clase que modela un sistema de informacion de peliculas 
 * donde son vistas por usuarios que la pueden calificar y 
 * donde hay administradores que se encargan de mantener actualizado Moovies
 * @author Victor Degano
 */
public class Moovies extends Observable
{
	/* Atributos/Variables */
	private HashMap<Integer, Pelicula> baseDePeliculas;
	private HashMap<Integer, Usuario> baseDeUsuarios;
	private ArrayList<Administrador> administradores;
	
	/*Constructores*/
	public Moovies()
	{
		this.baseDePeliculas	= new HashMap<Integer, Pelicula>();
		this.baseDeUsuarios		= new HashMap<Integer, Usuario>();
		this.administradores	= new ArrayList<Administrador>();
	}
	
	/*Getters & Setters*/
	private HashMap<Integer, Usuario> getUsuarios()
	{		
		return this.baseDeUsuarios;
	}
	
	private HashMap<Integer, Pelicula> getPeliculas()
	{
		return this.baseDePeliculas;
	}
	
	private ArrayList<Administrador> getAdministradores()
	{
		return this.administradores;
	}
	
	
	/*Metodos Para Test*/
	/**
	 * Retorna true si hay calificaciones hechas.
	 * @return Boolean - True en el caso de que se encuentren ratings, false en el caso contrario.
	 * @author Victor Degano
	 */
	public boolean hayRatings()
	{
		ArrayList<Pelicula> peliculasARevisar	= this.peliculas();
		
		for (Pelicula pelicula : peliculasARevisar) 
		{
			if (pelicula.hayRatings())
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Retorna true si hay amistades.
	 * @return Boolean - True en el caso de que se encuentren amistades, false en el caso contrario.
	 * @author Victor Degano
	 */
	public boolean hayAmigos()
	{
		ArrayList<Usuario> usuarioARevisar	= this.usuarios();
		
		for (Usuario usuario : usuarioARevisar) 
		{
			if(usuario.tieneAmigos())
			{
				return true;
			}	
		}
		return false;
	}
	
	
	/*Metodos*/
	/**
	 * Devuelve una ArrayList de todos los usuarios del sistema.
	 * @return ArrayList<Usuario> que contiene todos los usuarios
	 * @author Victor Degano
	 */
	public ArrayList<Usuario> usuarios()
	{
		return new ArrayList<Usuario>(this.getUsuarios().values());
	}
	
	/**
	 * Devuelve una ArrayList de todas las peliculas del sistema.
	 * @return ArrayList<Pelicula> que contiene todas las peliculas
	 * @author Victor Degano
	 */
	public ArrayList<Pelicula> peliculas()
	{
		return new ArrayList<Pelicula>(this.getPeliculas().values());
	}
	
	/**
	 * Devuelve una ArrayList de todos los administradores de moovies.
	 * @return ArrayList<Administradores> que contiene a todos los administradores
	 * @author Victor Degano
	 */
	public ArrayList<Administrador> administradores()
	{
		return this.getAdministradores();
	}
	
	/**
	 * Agrega a unUsuario a la base de datos de usuarios de moovies.
	 * <pre>Precondicion: el usuario debe ser consistente y acorde a moovies</pre>
	 * @param unUsuario - El usuario a agregar a moovies
	 * @author Victor Degano
	 */
	public void agregarUsuario(Usuario unUsuario, Integer id)
	{
		Usuario usuarioAAgregar	= unUsuario;
		usuarioAAgregar.nivelDePrivilegios(new NivelUsuario(this));
		this.getUsuarios().putIfAbsent(id, usuarioAAgregar);
	}
	
	/**
	 * Agrega a unaPelicula a la base de datos de peliculas de moovies con la id pasada. Si la Id ya esta 
	 * en uso no agrega la pelicula.
	 * <pre>Precondicion: la pelicula debe ser conistente y acorde a moovies</pre>
	 * @param id - La id Correspondiente a la pelicula a agregar.
	 * @param unaPelicula - La pelicula a agregar a moovies
	 * @author Victor Degano
	 */
	public void agregarPelicula(Pelicula unaPelicula, Integer id)
	{ 
		if (! this.getPeliculas().containsValue(unaPelicula))
		{
			this.getPeliculas().put(id, unaPelicula);
			this.setChanged();
			this.notifyObservers(unaPelicula);
		}
	}
	
	/**
	 * Agrega a unAdministrador a la base de datos de administradores de moovies.
	 * <pre>Precondicion: el administrador debe ser conistente y acorde a moovies</pre>
	 * @author Victor Degano
	 * @param unAdministrador - Es el administrador a agregar a moovies
	 */
	public void agregarAdministrador(Administrador unAdministrador)
	{
		Administrador administradorAAgregar	= unAdministrador;
		administradorAAgregar.nivelDePrivilegios(new NivelAdministrador(this));
		this.getAdministradores().add(unAdministrador);
	}

	/**
	 * Busca a un Usuario de Moovies por se Nombre y su Apellido. Regresa la primera 
	 * ocurrencia de un usuario que tenga ese nombre y apellido.
	 * @param unNombre - Es el nombre del usuario que queremo buscar
	 * @param unApellido - Es el apellido del usuario que queremos bucar
	 * @return Usuario - El usuario que se busca
	 * @throws RuntimeException - Al no encontrar un usuario con los parametros pasados
	 * @author Victor Degano
	 */
	public Usuario buscarUsuario(String unNombre, String unApellido)
	{
		ArrayList<Usuario> baseUsuariosABuscar	= new ArrayList<Usuario>(this.baseDeUsuarios.values());
		
		for (Usuario usuario : baseUsuariosABuscar) 
		{
			if (usuario.nombreCompleto().equals(unNombre + " " + unApellido))
			{
				return usuario;
			}
		}
		throw new RuntimeException	("No se encontro al usuario, puede que se "
				+ "haya ingresado mal lo datos o no este en la " 
				+ "base de datos");
	}
	
	/**
	 * Busca a una pelicula de Moovies por su nombre, si es igual al nombre 
	 * ingresado lo devuelve.
	 * @param unNombreDePelicula - Es el nombre de  la pelicula que queremos bucar
	 * @return Pelicula - La Pelicula que se busca
	 * @throws RuntimeException - Al no encontrar la pelicula con los parametros pasados
	 * @author Victor Degano
	 */	
	public Pelicula buscarPelicula(String unNombreDePelicula)
	{
		for (Pelicula pelicula : this.getPeliculas().values()) 
		{
			if (pelicula.titulo().equals(unNombreDePelicula))
			{
				return pelicula;
			}
		}
		throw new RuntimeException("Pelicula no encontrada, puede que e haya ingresado mal el nombre de la pelicula");
	}
	
//	/**
//	 * Revisa que unaPelicula este en la base de datos de peliculas de moovies.
//	 * @param unaPelicula - la pelicula que se quiere averiguar que esta en moovies, 
//	 * tiene que ser del tipo {@link PeliculaMoovie}
//	 * @return Boolean - True si esta la pelicula en la lista, False caso contrario
//	 * @author Victor Degano
//	 */
//	public boolean estaLaPelicula(Pelicula unaPelicula)
//	{
//		for (Pelicula pelicula : this.getPeliculas().values()) 
//		{
//			if (this.sonLaMismaPelicula(pelicula, unaPelicula))
//			{
//				return true; 
//			}
//		} 
//		return false;
//	}

	/**
	 * Revisa que este la ID correspondiente a una pelicula en la base de datos de peliculas de moovies.
	 * @param unIdDePelicula - Es el int de la id de la pelicula que se quiere averiguar que esta en moovies.
	 * @return Boolean - True si esta la id, False caso contrario.
	 * @author Victor Degano
	 */
	public boolean estaLaPeliculaConId(int unIdDePelicula)
	{
		return this.getPeliculas().containsKey(unIdDePelicula);
	}
	
	/**
	 * Revisa que este una pelicula en la base de datos de peliculas de moovies.
	 * @param unaPelicula - Es {@link Pelicula} que se quiere averiguar que esta en moovies.
	 * @return Boolean - True si esta la pelicula, False caso contrario.
	 * @author Victor Degano
	 */
	public boolean estaLaPelicula(Pelicula unaPelicula)
	{
		return this.getPeliculas().containsValue(unaPelicula);
	}
	
	/**
	 * Revisa que este el usuario con la id idUsuario en moovies.
	 * @param idUsuario - El Id del usuario a buscar.
	 * @return Boolean - True si esta el usuario , False caso contrario
	 * @author Victor Degano
	 */
	public boolean estaElUsuarioConId(int idUsuario)
	{
		return this.getUsuarios().containsKey(idUsuario);
	}
	
	/**
	 * Busca a un Usuario de Moovies por si Id. 
	 * @param unID - Es el id unico del usuario que se busca.
	 * @return Usuario - El usuario que se busca
	 * @throws RuntimeException - Al no encontrar un usuario
	 */
	public Usuario buscarUsuarioPorID(Integer unID)
	{
		if (this.getUsuarios().containsKey(unID))
		{
			return this.getUsuarios().get(unID);
		}
		throw new RuntimeException	("No se encontro al usuario correspondiente a dicha ID");
	}

	/**
	 * Busca a una pelicula de Moovies por si Id. 
	 * @param unID - Es el id unico de la pelicula que se busca.
	 * @return Pelicula- El usuario que se busca
	 * @throws RuntimeException - Al no encontrar un usuario
	 */
	public Pelicula buscarPeliculaPorID(Integer unID)
	{
		if (this.getPeliculas().containsKey(unID))
		{
			return this.getPeliculas().get(unID);
		}
		throw new RuntimeException	("No se encontro la pelicula correspondiente a dicha ID");
	}
	
	/**
	 * Devuelve la lista de los 10 usuarios mas activos, ordenados del mas 
	 * activo al menos activo. En caso que no haya usuarios o haya menos de 
	 * 10 usuarios, devuelve una lista con los usuarios que haya.
	 * 
	 * @return ArrayList<Usuario> - Es la lista de los 10 usuarios mas 
	 * activos del sistema, la lista tiene una capacidad maxima de 10 Usuarios.
	 * @author Victor Degano
	 */
	public ArrayList<Usuario> usuariosMasActivos()
	{
		ArrayList<Usuario> listaDiezMasActivos;
		ArrayList<Usuario> baseDeUsuariosAComparar		= this.usuarios();
		baseDeUsuariosAComparar.sort(new OrdenarUsuariosPorCantidadDeCalificaciones());
		
		if (baseDeUsuariosAComparar.size() > 10)
		{
			listaDiezMasActivos	= new ArrayList<Usuario>(baseDeUsuariosAComparar.subList(0, 10));
			return listaDiezMasActivos;
		}
		return baseDeUsuariosAComparar;
	}

	/**
	 * Guarda la calificacion hecha por el usuario tanto en la pelicula como en el usuario
	 * <pre>Precondicion: El Valor de la calificacion tiene que ser un numero entre 1 y 5</pre>
	 * @param unaPelicula - Es la pelicula que se puntua
	 * @param unUsuario - Es el usuario que califica la pelicula
	 * @param calificacion - Es el puntaje dado a la pelicula
	 * @author Victor Degano
	 */
	public void calificarPeliculaPorUsuario(Pelicula unaPelicula, Usuario unUsuario, Integer calificacion)
	{
		int unIdDeUsuario	= 0;
		int unIdDePelicula	= 0;
				
		//Busca el Id del Usuario
		unIdDeUsuario	= this	.getUsuarios()
								.entrySet()
								.stream()
								.filter(e -> e.getValue().equals(unUsuario))
								.map(e -> e.getKey())
								.findFirst().get();
				
		//Busca el Id de la pelicula
		unIdDePelicula	= this	.getPeliculas()
								.entrySet()
								.stream()
								.filter(e -> e.getValue().equals(unaPelicula))
								.map(e -> e.getKey())
								.findFirst().get();
		
		Rating nuevoRating	= new Rating(unIdDeUsuario, LocalDate.now(), unIdDePelicula, calificacion);
		nuevoRating.agregarNombreDePelicula(unaPelicula.titulo());
		nuevoRating.agregarNombreDeUsuario(unUsuario.nombreCompleto());
		nuevoRating.agregarPeliculaIMDB(unaPelicula.imdb());
		unUsuario.guardarRating(nuevoRating);
		unaPelicula.guardarRating(nuevoRating);
	}

	/**
	 * Devuelve la lista de las 10 peliculas mejor calificadas, ordenados de la mejor 
	 * calificada a la peor calificada. En caso que no haya peliculas o haya menos de 
	 * 10, devuelve una lista con las peliculas que haya.
	 * 
	 * @return List<Pelicula> - Es la lista de las 10 peliculas mejor calificadas 
	 * del sistema, la lista tiene una capacidad maxima de 10 Peliculas.
	 * @author Victor Degano
	 */
	public ArrayList<Pelicula> mejoresPeliculas()
	{
		ArrayList<Pelicula> diezMejoresPeliculas;
		ArrayList<Pelicula> baseDePeliculasAComparar	= this.peliculas();
		
		baseDePeliculasAComparar.sort(new OrdenarPeliculaPorPromedio());
		
		if (baseDePeliculasAComparar.size() > 10)
		{
			diezMejoresPeliculas	= new ArrayList<Pelicula>(baseDePeliculasAComparar.subList(0, 10));
			return diezMejoresPeliculas;
		}
		return baseDePeliculasAComparar;
	}
	
	//PROPOSITO: Dado un imdb, retorna la pelicula al cual pertenece. Si no existe, retorna un mensaje de Exception
	public Pelicula buscarPeliculaPorIMDB(String imdb) 
	{
		for (Pelicula pelicula : this.peliculas()) 
		{
			if (pelicula.imdb().equals(imdb))
			{
				return pelicula;
			}
		}throw new RuntimeException("imdb no encontrado");
	}

	//PROPOSITO: Dado un listado de imdbs de peliculas del sistema, retorna las peliculas 
	//  	     correspondientes a los imdbs de la lista.
	public ArrayList<Pelicula> listarPeliculas(ArrayList<String> imdbs) 
	{
		ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
		for(String imdb : imdbs)
		{
			peliculas.add(this.buscarPeliculaPorIMDB(imdb));
		}
		return peliculas;
	}

}