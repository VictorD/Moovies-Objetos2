package sistemaMoovies;

import java.util.ArrayList;

public abstract class GeneroEspecifico extends Genero
{
	//Constructor
	public GeneroEspecifico(String unNombre) 
	{
		super(unNombre);
	}
	
	//Metodos
	public GeneroEspecifico compose(GeneroEspecifico unGeneroEspecifico)
	{
		GeneroEspecifico nuevoGeneroCompuesto	= new GeneroEspecificoCompuesto(this.nombre(), unGeneroEspecifico);
		return nuevoGeneroCompuesto;
	}
	
	public abstract GeneroEspecifico agregarSubGenero(GeneroEspecifico unSubGenero);

	public abstract ArrayList<String> listarGeneros();

}
