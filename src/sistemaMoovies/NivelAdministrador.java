package sistemaMoovies;

import java.util.HashMap;
import java.util.List;

import csvFileReader.Contactos;
import csvFileReader.RatingIMDBFileReader;

public class NivelAdministrador extends NivelDePrivilegios
{
	//Atributos
	
	//Constructores
	public NivelAdministrador(Moovies unSistema) 
	{
		super(unSistema);
	}

	/**
	 * Toma una lista de peliculas y las agrega a la base de peliculas de Moovies que se administra. 
	 * La lista es un contenedor el cual guarda la pelicula y la Id correspondiente a este. 
	 * Si hay peliculas que ya se encuentran en moovies, no las agrega.
	 * <pre>Precondicion: La lista debe tener peliculas consistentes y acordes a Moovies.</pre>
	 * @param listaDePeliculas - Es la lista de los contenedores de las peliculas que se van a agregar al sistema
	 * @author Victor Degano
	 */
	public void cargarPeliculasImportadas(List<ContenedorDePelicula> listaDePeliculasAImportar)
	{
		List<ContenedorDePelicula> pelisAAgregar	= listaDePeliculasAImportar;
		ContenedorDePelicula peliAAgregar;
		for (int i = 0; i < pelisAAgregar.size(); i++) 
		{
			peliAAgregar	= pelisAAgregar.get(i);
			this.sistema().agregarPelicula(peliAAgregar.pelicula(), peliAAgregar.id());
		}		
	}

	/**
	 * Toma una lista de usuarios y las agrega a la base de usuarios de Moovies que se administra. 
	 * La lista es un contenedor el cual guarda al usuario y la Id correspondiente a este. 
	 * Si hay usuarios que ya se encuentran en moovies, no las agrega.
	 * <pre>Precondicion: La lista debe tener Usuarios consistentes y acordes a Moovies.</pre>
	 * @param listaDeUsuarios - Es la lista de los contenedores de las usuarios que se van a agregar al sistema
	 * @author Victor Degano
	 */
	public void cargarUsuariosImportados(List<ContenedorDeUsuario> listaDeUsuarios)
	{
		List<ContenedorDeUsuario> usuariosAAgregar = listaDeUsuarios;
		ContenedorDeUsuario usuarioAAgregar;
		for (int i = 0; i < usuariosAAgregar.size(); i++) 
		{
			usuarioAAgregar	= usuariosAAgregar.get(i);
			this.sistema().agregarUsuario(usuarioAAgregar.usuario(), usuarioAAgregar.id());
		}		
	}

	/**
	 * Toma una lista de ratings y las importa al sistema completando el nombre del usuario y la pelicula
	 * si ya hay ratings cargados en el sistema, no los carga.
	 * @param listaDeRatings - la lista de ratings que se quiere importar
	 * @author Victor Degano
	 */
	public void cargarRatingsImportados(List<Rating> listaDeRatings)
	{
		int tamanioListaRating	= listaDeRatings.size(); 
		
		Rating ratingAGuardar;
		
		for (int i = 0; i < tamanioListaRating; i++) 
		{
			ratingAGuardar	= listaDeRatings.get(i);
			int	idUsuario	= ratingAGuardar.idUsuario();
			int idPelicula	= ratingAGuardar.peliculaId();
			
			Usuario usuarioCalificador;
			Pelicula peliculaCalificada;
			
			if (	this.sistema().estaElUsuarioConId(idUsuario) &&
					this.sistema().estaLaPeliculaConId(idPelicula))
			{
				//Busca al usuario y pelicula.
				peliculaCalificada = this.sistema().buscarPeliculaPorID(idPelicula);
				usuarioCalificador = this.sistema().buscarUsuarioPorID(idUsuario);
				//Agrega los nombres al rating.
				ratingAGuardar.agregarNombreDePelicula(peliculaCalificada.titulo());
				ratingAGuardar.agregarNombreDeUsuario(usuarioCalificador.nombreCompleto());
				ratingAGuardar.agregarPeliculaIMDB(peliculaCalificada.imdb());
				//Guarda tanto en usuario como en pelicula el rating.
				usuarioCalificador.guardarRating(ratingAGuardar);
				peliculaCalificada.guardarRating(ratingAGuardar);
			}
		}
	}

	/**Toma una lista de Id de usuarios y los relaciona para recrear las amistades importadas. Si ya 
	 * se encuentra una amistada cargada, no la importa.
	 * <pre>Precondicion: La lista debe ser de tamaño Par para importar correctamente todas las amistades</pre>
	 * @param unaListaDeAmigos - La lista de id de amigos que importa las relaciones de amistad
	 */
	public void cargarAmigosImportados(List<Contactos> unaListaDeAmigos)
	{
		Usuario usuario1;
		Usuario usuarioAmigo;
		int iterador = 0;
		
		while(iterador < unaListaDeAmigos.size())
		{
			try 
			{
				usuario1	= this.sistema().buscarUsuarioPorID(unaListaDeAmigos.get(iterador).usuarioID());
				usuarioAmigo= this.sistema().buscarUsuarioPorID(unaListaDeAmigos.get(iterador).amigoID());
				if (! usuario1.amigoDe(usuarioAmigo))
				{
					usuario1.agregarComoAmigo(usuarioAmigo);
					usuarioAmigo.agregarComoAmigo(usuario1);
				}	
			} 
			catch (RuntimeException e) 
			{
				//No hace nada con la exepcion, lo maneja no agregando como amigos a esos usuarios
			}
			iterador = iterador + 2;
		}
	}

	//Carga al sistema los ratings IMDB de las peliculas.
	//Se asume que idPelicula existe en moovies.
	public void cargarRatingsIMDBImportados(List<Rating> ratingsAImportar) 
	{
		RatingIMDBFileReader calculadorRatingsIMDB = new RatingIMDBFileReader();
		HashMap<Integer, Integer> mapPeliculaRating = new HashMap<Integer, Integer>();
		mapPeliculaRating = calculadorRatingsIMDB.cargarRatingsIMDBImportados(ratingsAImportar);
		for(int id : mapPeliculaRating.keySet())
		{
			this.sistema().buscarPeliculaPorID(id).actualizarRatingIMDB(mapPeliculaRating.get(id));
		}
	}
	
}
