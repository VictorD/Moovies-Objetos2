package sistemaMoovies;

/**
 * Clase que representa a una persona que es parte del sistema moovies
 * @author Victor Degano
 */
public abstract class Persona 
{

	//Atributos
	private String apellido;
	private String nombre;
	private Integer edad;
	private String ocupacion;
	private String codigoPostal;
	
	//Constructor
	public Persona(String unNombre, String unApellido, Integer unaEdad, String unaOcupacion, String unCodigoPostal)
	{
		this.nombre				= unNombre;
		this.apellido			= unApellido;
		this.edad				= unaEdad;
		this.ocupacion			= unaOcupacion;
		this.codigoPostal		= unCodigoPostal;
	}
	
	//Getter && Setter
	private String getNombre() 
	{
		return this.nombre;
	}

	private String getApellido() 
	{
		return this.apellido;
	}
	
	private Integer getEdad() 
	{
		return this.edad;
	}
	
	private String getOcupacion() 
	{
		return this.ocupacion;
	}
	
	private String getCodigoPostal() 
	{
		return this.codigoPostal;
	}
	
	//Metodos
	/**
	 * Regresa el nombre de la Persona.
	 * @return String - Que representa el nombre de la persona.
	 * @author Victor Degano
	 */
	public String nombre() 
	{
		return this.getNombre();
	}

	/**
	 * Regresa el apellido de la persona.
	 * @return String - Que representa el apellido de la persona.
	 * @author Victor Degano
	 */
	public String apellido() 
	{
		return this.getApellido();
	}
	
	/**
	 * Regresa la edad de la persona.
	 * @return Integer - Que representa la edad de la persona.
	 * @author Victor Degano
	 */
	public Integer edad() 
	{
		return this.getEdad();
	}
	
	/**
	 * Regresa la ocupacion de la persona.
	 * @return String - Que representa la ocupacion de la persona.
	 * @author Victor Degano
	 */
	public String ocupacion() 
	{
		return this.getOcupacion();
	}
	
	/**
	 * Regresa el codigo postal de la persona.
	 * @return String - Que representa el codigo postal de la persona.
	 * @author Victor Degano
	 */
	public String codigoPostal() 
	{
		return this.getCodigoPostal();
	}
}
