package sistemaMoovies;

import java.util.Comparator;

public class OrdenarPeliculaPorRatingIMDB implements Comparator<Pelicula> {

	@Override
	public int compare(Pelicula o1, Pelicula o2) 
	{
		if (o1.ratingIMDB() > o2.ratingIMDB())
		{
			return -1;
		}
		else if (o1.ratingIMDB() < o2.ratingIMDB())
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

}