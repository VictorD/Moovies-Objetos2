package sistemaMoovies;

import java.util.ArrayList;

public class GeneroEspecificoCompuesto extends GeneroEspecifico
{
	//Atributos
	private ArrayList<GeneroEspecifico> subGeneros;
	
	//Constructor
	public GeneroEspecificoCompuesto(String unNombre, GeneroEspecifico unSubGenero) 
	{
		super(unNombre);
		ArrayList<GeneroEspecifico> subGeneros	= new ArrayList<GeneroEspecifico>();
		subGeneros.add(unSubGenero);
		this.setSubGeneros(subGeneros);
	}

	//Getter & Setter
	private void setSubGeneros(ArrayList<GeneroEspecifico> unaListaDeSubGeneros)
	{
		this.subGeneros	= unaListaDeSubGeneros;
	}
	
	private ArrayList<GeneroEspecifico> getSubGeneros()
	{
		return this.subGeneros;
	}
	
	//Metodos
	@Override
	public GeneroEspecifico agregarSubGenero(GeneroEspecifico unSubGenero) 
	{
		this.getSubGeneros().add(unSubGenero);
		return this;
	}

	@Override
	public ArrayList<String> listarGeneros() 
	{
		ArrayList<String> listaDeGeneros	= new ArrayList<String>();
		listaDeGeneros.add(this.nombre());
		
		for (GeneroEspecifico subGenero : this.getSubGeneros()) 
		{
			listaDeGeneros.addAll(subGenero.listarGeneros());			
		}

		return listaDeGeneros;
	}

}
