package sistemaMoovies;

import java.util.ArrayList;

public class GeneroEspecificoSimple extends GeneroEspecifico
{
	//Constructor
	public GeneroEspecificoSimple(String unNombre)
	{
		super(unNombre);
	}
	
	//Metodos
	@Override
	public ArrayList<String> listarGeneros() 
	{
		ArrayList<String> listaDeGeneros	= new ArrayList<String>();
		listaDeGeneros.add(this.nombre());
		return listaDeGeneros;
	}
	
	@Override
	public GeneroEspecifico agregarSubGenero(GeneroEspecifico unSubGenero) 
	{
		GeneroEspecifico nuevoGeneroEspecificoCompuesto	= new GeneroEspecificoCompuesto(this.nombre(), unSubGenero);
		return nuevoGeneroEspecificoCompuesto;
	}

}
