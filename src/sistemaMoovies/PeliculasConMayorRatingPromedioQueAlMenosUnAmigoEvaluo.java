package sistemaMoovies;

import java.util.ArrayList;


public class PeliculasConMayorRatingPromedioQueAlMenosUnAmigoEvaluo implements MetodoDeRecomendacionDePeliculas
{
	//Cuando un usuario selecciona este metodo de recomendacion, se recomendaran aquellas peliculas
	//que tengan el mayor rating promedio y que al menos un amigo haya avaluado.
	
	@Override
	public ArrayList<Pelicula> filtrarRecomendadas(Usuario usuario, ArrayList<Usuario> amigos) 
	{
		ArrayList<Pelicula> peliculasDeUsuarios = new ArrayList<Pelicula>();
		ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
		
		for(Usuario amigo : amigos)
		{
			peliculasDeUsuarios.addAll(amigo.peliculasCalificadas());
		}
		
		for(Pelicula pelicula : peliculasDeUsuarios)
		{
			if(pelicula.promedio() == 5 && !peliculas.contains(pelicula))
			{
				peliculas.add(pelicula);
			}
		}
		
		return peliculas;
	}

}
