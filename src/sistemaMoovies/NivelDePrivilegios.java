package sistemaMoovies;

public abstract class NivelDePrivilegios 
{
	//Variables
	private Moovies sistema;
	
	//Constructor
	public NivelDePrivilegios(Moovies unSistema)
	{
		this.sistema	= unSistema;
	}
	
	//Getter && Setter
	private Moovies getSistema()
	{
		return this.sistema;
	}
	
	//Metodos
	public Moovies sistema()
	{
		return this.getSistema();
	}
	
}
