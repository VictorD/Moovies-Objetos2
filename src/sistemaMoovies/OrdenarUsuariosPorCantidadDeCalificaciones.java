package sistemaMoovies;

import java.util.Comparator;

/**
 * Clase que implementa la interfaz Comparator, para poder comparar usuarios por su 
 * cantidad de calificaciones hechas
 * @author Victor Degano
 */
public class OrdenarUsuariosPorCantidadDeCalificaciones implements Comparator<Usuario> 
{
	
	/**
	 * Compara dos Usuarios (o1 y o2) por la cantidad de calificaciones hechas.
	 * @param o1 - Es el primer usuario que se va a comparar.
	 * @param o2 - Es el segundo usuario que se va a comparar.
	 * @return Int 	- Devuelve -1 si la cantidad de calificacions de o1 > la cantidad de calificacions de o2 
	 * 				- Devuelve 1 si la cantidad de calificacions de o1 < la cantidad de calificacions de o2
	 * 				- Devuelve 0 si la cantidad de calificacions de o1 = la cantidad de calificacions de o2
	 * @author Victor Degano
	 */
	@Override
	public int compare(Usuario o1, Usuario o2) 
	{
		if(o1.cantidadDeCalificaciones() > o2.cantidadDeCalificaciones())
		{
			return -1;
		}
		else if (o1.cantidadDeCalificaciones() < o2.cantidadDeCalificaciones()) 
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

}
