package sistemaMoovies;

import java.util.ArrayList;
import java.util.HashMap;

public class Usuario extends Persona
{
	//Atributos
	private ArrayList<Rating> ratings;
	private ArrayList<Usuario> amigos;
	private NivelUsuario nivelDePrivilegios;
	private ArrayList<Pelicula> nuevasPeliculasDeInteres;
	private MetodoDeRecomendacionDePeliculas metodoDeRecomendacionDePeliculas;
	
	/*Constructores*/		
	public Usuario(String unNombre,String unApellido, int unaEdad, String unaOcupacion, String unCodigoPotal)
	{
		super(unNombre, unApellido, unaEdad, unaOcupacion, unCodigoPotal);
		this.setNivelDePrivilegios(new NivelUsuario(new Moovies()));
		this.ratings							= new ArrayList<Rating>();
		this.amigos								= new ArrayList<Usuario>();
		this.nuevasPeliculasDeInteres			= new ArrayList<Pelicula>();
		this.metodoDeRecomendacionDePeliculas	= new PeliculasCalificadasCon4OMasPorMasDeLaMitadDeAmigosQueUsuarioAunNoEvaluo() ;
	}
	
	//Getter && Setters
	private ArrayList<Usuario> getAmigos()
	{
		return this.amigos;
	}
	
	private ArrayList<Rating> getRatings()
	{
		return this.ratings;
	}
	
	private void setNivelDePrivilegios(NivelUsuario unNivelDePrivilegios)
	{
		this.nivelDePrivilegios	= unNivelDePrivilegios;
	}
	
	private ArrayList<Pelicula> getNuevasPeliculasDeInteres()
	{
		return this.nuevasPeliculasDeInteres;
	}
	
	private MetodoDeRecomendacionDePeliculas getMetodoDeRecomendacion() 
	{
		return this.metodoDeRecomendacionDePeliculas;
	}
	
	private void setMetodoDeRecomendacion(MetodoDeRecomendacionDePeliculas unMetodo) 
	{
		this.metodoDeRecomendacionDePeliculas = unMetodo;
	}
	
	private NivelUsuario getNivelDePrivilegios() {
		return this.nivelDePrivilegios;
	}

	
	/*Metodos*/	
	public void nivelDePrivilegios(NivelUsuario unNivelDePrivilegios)
	{
		this.setNivelDePrivilegios(unNivelDePrivilegios);
	}
	
	public ArrayList<Usuario> amigos()
	{
		return this.getAmigos();
	}
	
	public ArrayList<Rating> ratings()
	{
		return this.getRatings();
	}
	
	public boolean tieneCalificaciones() 
	{
		return this.ratings().size() > 0;
	}
	
	private boolean estaRating(Rating unRating)
	{
		return this.ratings().contains(unRating);
		
	}
	
	/*Si esta el rating cargado no lo guarda*/
	public void guardarRating(Rating ratingAGuardar)
	{
		if (! this.estaRating(ratingAGuardar))
		{
			this.getRatings().add(ratingAGuardar);
		}
	}
	
	public boolean tieneAmigos()
	{
		return this.getAmigos().size() > 0;
	}

	public boolean amigoDe(Persona  usuarioAmigo)
	{
		return this.getAmigos().contains(usuarioAmigo);
	}

	//Precondicion: No tiene que estar el usuario a registrar como amigo en la lista de amigos.
	public void agregarComoAmigo(Usuario usuarioAmigo)
	{
		ArrayList<Usuario> misAmigos	= this.getAmigos();
		
		if (! misAmigos.contains(usuarioAmigo))
		{
			misAmigos.add(usuarioAmigo);
			usuarioAmigo.amigos().add(this);
		}	
	}
	
	public int cantidadDeCalificaciones()
	{
		return this.ratings.size();
	}
	
	//@throws RuntimeException - Al no encontrar la pelicula con los parametros pasados
	public Pelicula buscarPelicula(String unaPelicula)
	{
		return getNivelDePrivilegios().buscarPelicula(unaPelicula);
		
	}
	
	//@throws RuntimeException - Al no encontrar al usuario con los parametros pasados
	public Usuario buscarUsuario(String unNombre, String unApellido)
	{
		return getNivelDePrivilegios().buscarUsuario(unNombre, unApellido);
	}
	
	public ArrayList<Rating> misPuntajes()
	{   
		return this.ratings();
	}	
	
	public ArrayList<Usuario> verUsuariosMasActivos()
	{
		return getNivelDePrivilegios().diezUsuariosMasActivos();
	}
	
	public ArrayList<Pelicula> verMejores10Peliculas()
	{
		return getNivelDePrivilegios().mejoresDiezPeliculas();
	}
		
	public ArrayList<String> misDatos()
	{
		ArrayList<String> datosUsuario	= new ArrayList<String>();
		
		datosUsuario.add(this.nombre());
		datosUsuario.add(this.apellido());
		datosUsuario.add(this.edad().toString());
		datosUsuario.add(this.codigoPostal());
		datosUsuario.add(this.ocupacion());
		return datosUsuario;
	}
	
	public ArrayList<String> verDatosAmigo(Usuario unUsuario)
	{
		for (Usuario usuario : amigos) 
		{
			if (usuario == unUsuario)
			{
				return usuario.misDatos();
			}
		}
		throw new RuntimeException	("No se puede ver la informacion del usuario, puede que no lo tenga como amigo");
	}
	
	public void calificarPelicula(Pelicula unaPelicula,int nota)
	{
		if (getNivelDePrivilegios().estaLaPelicula(unaPelicula))
		{
			getNivelDePrivilegios().calificarPeliculaPorUsuario(unaPelicula,this ,nota); 
		}
	}
	
	public ArrayList<String> verInformacionDePelicula (Pelicula unaPelicula)
	{
		return unaPelicula.informacionDePelicula();	
	}
	
	public ArrayList<Pelicula> peliculasCalificadas()
	{
		return getNivelDePrivilegios().peliculasCalificadasPor(this);
	}
	
	public int cantidadDePeliculasCalificadas()
	{
		return this.peliculasCalificadas().size();
	}
	
	public HashMap<String,Integer> verCalificacionesDePeliculas()
	{
		return getNivelDePrivilegios().verCalificacionesDePeliculasDeUsuario(this);
	}

	public String nombreCompleto() 
	{
		return this.nombre() + " " + this.apellido();
	}
	
	public ArrayList<Pelicula> nuevasPeliculasDeInteres()
	{
		return this.getNuevasPeliculasDeInteres();
	}

	public void agregarPeliculaDeInteres(Pelicula pelicula1) 
	{
		ArrayList<Pelicula> listadoDeNuevasPeliculas	= this.getNuevasPeliculasDeInteres();		
		if(! listadoDeNuevasPeliculas.contains(pelicula1))
		{
			this.getNuevasPeliculasDeInteres().add(pelicula1);
		}
	}

	public void suscribirseA(ArrayList<Genero> unaListaDeGeneros) 
	{
		this.getNivelDePrivilegios().suscribirseA(unaListaDeGeneros, this);
	}

	//Proposito: Retorna las peliculas cuyo puntaje dado por el usuario
	//			 sea igual o mayor al puntaje pasado por parametro.
	public ArrayList<Pelicula> peliculasCalificadasConPuntajeMinimo(int puntaje) 
	{
		ArrayList<String> imdbs = new ArrayList<String>();
		
		for(Rating rating : this.misPuntajes())
		{
			if(rating.calificacion() >= puntaje)
			{
				imdbs.add(rating.peliculaImdb());
			}
		}
		
		return nivelDePrivilegios.listarPeliculas(imdbs);
	}
	
	public MetodoDeRecomendacionDePeliculas metodoDeRecomendacion() 
	{
		return this.getMetodoDeRecomendacion();
	}

	public void seleccionarMetodoDeRecomendacionDePeliculas(MetodoDeRecomendacionDePeliculas unMetodo) 
	{
		this.setMetodoDeRecomendacion(unMetodo);
	}

	public ArrayList<Pelicula> peliculasRecomendadas() 
	{
		return this.metodoDeRecomendacionDePeliculas.filtrarRecomendadas(this, this.amigos);
	}
}
