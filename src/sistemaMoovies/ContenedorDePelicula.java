package sistemaMoovies;

public class ContenedorDePelicula {

	private int idPeliculaAImportar;
	private Pelicula peliculaAImportar;

	//Constructor
	public ContenedorDePelicula (int unId, Pelicula unaPelicula)
	{
		this.idPeliculaAImportar	= unId;
		this.peliculaAImportar	= unaPelicula;
	}

	//Getter & Setter
	private int getIdPeliculaAImportar() 
	{
		return this.idPeliculaAImportar;
	}
	
	private Pelicula getPeliculaAImportar() 
	{
		return this.peliculaAImportar;
	}
	
	//Metodos
	public int id() 
	{
		return this.getIdPeliculaAImportar();
	}

	public Pelicula pelicula() 
	{
		return this.getPeliculaAImportar();
	}

}
