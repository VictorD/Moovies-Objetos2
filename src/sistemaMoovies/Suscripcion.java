package sistemaMoovies;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Suscripcion implements Observer
{
	//Atributos
	private ArrayList<Genero> generosSuscriptos;
	private Usuario	suscriptor;
	
	//Constructores
	public Suscripcion(ArrayList<Genero> generosASuscribirse, Usuario unSuscriptor)
	{
		this.setGenerosSuscriptos(generosASuscribirse);
		this.setSuscriptor(unSuscriptor);
	}
	
	//Getter & Setter
	private void setSuscriptor(Usuario unSuscriptor)
	{
		this.suscriptor	= unSuscriptor;
	}
	
	private void setGenerosSuscriptos(ArrayList<Genero> generosASuscribirse)
	{
		this.generosSuscriptos	= generosASuscribirse;
	}

	private ArrayList<Genero> getGenerosSuscriptos()
	{
		return this.generosSuscriptos;
	}
	
	private Usuario getSuscriptor()
	{
		return this.suscriptor;
	}
	
	//Metodos
	public ArrayList<Genero> generosSuscriptos()
	{
		return this.getGenerosSuscriptos();
	}
	
	public Usuario suscriptor()
	{
		return this.getSuscriptor();
	}

	private ArrayList<String> nombreDeGenerosSuscriptos() 
	{
		ArrayList<String> nombreDeGenerosSuscriptos	= new ArrayList<String>();
		
		for (Genero genero : this.getGenerosSuscriptos()) 
		{
			nombreDeGenerosSuscriptos.addAll(genero.listarGeneros());
		}
		return nombreDeGenerosSuscriptos;
	}
	
	@Override
	public void update(Observable unaMoovies, Object unaPelicula) 
	{
		Pelicula peliculaARevisar				= (Pelicula) unaPelicula;
		ArrayList<String> generosDeLaPelicula	= peliculaARevisar.listaDeGeneros();
		
		ArrayList<String> nombreDeGenerosSuscriptos	= this.nombreDeGenerosSuscriptos();
		
		for (String nombreDeGeneroSuscripto : nombreDeGenerosSuscriptos) 
		{
			if(generosDeLaPelicula.contains(nombreDeGeneroSuscripto))
			{
				this.getSuscriptor().agregarPeliculaDeInteres(peliculaARevisar);
				break;
			}
		}
				
	}
}
