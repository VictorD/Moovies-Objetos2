package sistemaMoovies;

public class ContenedorDeUsuario 
{
	private int idUsuarioAImportar;
	private Usuario usuarioAImportar;

	//Constructor
	public ContenedorDeUsuario (int unId, Usuario unUsuario)
	{
		this.idUsuarioAImportar	= unId;
		this.usuarioAImportar	= unUsuario;
	}

	//Getter & Setter
	private int getIdUsuarioAImportar() 
	{
		return this.idUsuarioAImportar;
	}
	
	private Usuario getUsuarioAImportar() 
	{
		return this.usuarioAImportar;
	}
	
	//Metodos
	public int id() 
	{
		return this.getIdUsuarioAImportar();
	}

	public Usuario usuario() 
	{
		return this.getUsuarioAImportar();
	}

}
