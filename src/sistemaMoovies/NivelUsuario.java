package sistemaMoovies;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * Representa el nivel de privilegios de un {@linkplain Usuario} del sistema {@linkplain Moovies}
 * @author Victor Degano
 */
public class NivelUsuario extends NivelDePrivilegios 
{
	//Atributos	
	
	//Constructores
	public NivelUsuario(Moovies unSistema)
	{
		super(unSistema);
	}
	
	/**
	 * Dado un String devuelve la primera ocurrencia de una pelicula que contenga ese nombre.
	 * @param unaPelicula - El nombre de la pelicula que se quiere buscar
	 * @return Pelicula - La primera pelicula que cumpla con el requisito.
	 * @throws RuntimeException - Al no encontrar la pelicula con los parametros pasados
	 * @author Victor Degano
	 */
	public Pelicula buscarPelicula(String unaPelicula) 
	{
		return this.sistema().buscarPelicula(unaPelicula);
	}

	/**
	 * Dado un String que representa unNombre y un string que representa unApellido 
	 * devuelve la primera ocurrencia de un Usuario que contenga ese nombre y apellido.
	 * @param unaNombre - El nombre del usuario que se quiere buscar
	 * @param unaApellido - El apellido del usuario que se quiere buscar
	 * @return Usuario - El primer usuario que cumpla con el requisito.
	 * @throws RuntimeException - Al no encontrar el usuario con los parametros pasados
	 * @author Victor Degano
	 */
	public Usuario buscarUsuario(String unNombre, String unApellido) 
	{
		return this.sistema().buscarUsuario(unNombre, unApellido);
	}

	/**
	 * Devuelve la lista de los 10 Usuarios que mas activos, ordenados del usuario 
	 * que mas activo al que menos activo. Puede devolver una lista vacia o con menos 
	 * de 10 Usuarios.
	 * @return ArrayList<Usuario> - Es la lista de los 10 usuarios mas activos 
	 * del sistema, la lista tiene una capacidad maxima de 10 Usuarios.
	 * @author Victor Degano
	 */
	public ArrayList<Usuario> diezUsuariosMasActivos() 
	{
		return this.sistema().usuariosMasActivos();
	}

	/**
	 * Devuelve la lista de las 10 peliculas mejor calificadas, ordenados de la mejor 
	 * calificada a la peor calificada. Puede devolver una lista vacia o con menos 
	 * de 10 Peliculas.
	 * @return ArrayList<Pelicula> - Es la lista de las 10 peliculas mejor calificadas 
	 * del sistema, la lista tiene una capacidad maxima de 10 Peliculas.
	 * @author Victor Degano
	 */
	public ArrayList<Pelicula> mejoresDiezPeliculas() 
	{
		return this.sistema().mejoresPeliculas();
	}

	/**
	 * Retorna True si unaPelicula esta en el sistema.
	 * @param unaPelicula - La {@linkplain Pelicula} que se quiere saber 
	 * si ya esta en moovies 
	 * @return Boolean - True en caso de que este, false en caso contrario.
	 * @author Victor Degano
	 */
	public boolean estaLaPelicula(Pelicula unaPelicula) 
	{
		return this.sistema().estaLaPelicula(unaPelicula);
	}

	/**
	 * Realiza la calificacion de unaPelicula realizada por usuario con una nota.
	 * @param unaPelicula - La {@linkplain Pelicula} que se califico.
	 * @param usuario - El {@linkplain Usuario} que califico la pelicula. 
	 * @param nota - El Int que representa la nota
	 * @author Victor Degano
	 */
	public void calificarPeliculaPorUsuario(Pelicula unaPelicula, Usuario usuario, int nota) 
	{
		this.sistema().calificarPeliculaPorUsuario(unaPelicula, usuario, nota);
	}

	public ArrayList<Pelicula> peliculasCalificadasPor(Usuario usuario) 
	{
		ArrayList<Pelicula> peliculas= new ArrayList<Pelicula>();
		for (Rating rating : usuario.ratings()) 
		{
			int idPelicula 	= rating.peliculaId();
			Pelicula unaPelicula 	= this.sistema().buscarPeliculaPorID(idPelicula);
			peliculas.add(unaPelicula);	
		}
		return peliculas;
	}

	public HashMap<String, Integer> verCalificacionesDePeliculasDeUsuario(Usuario usuario) 
	{
		HashMap<String , Integer > peliculaYCalificacion = new HashMap<String,Integer>();
		for (int i = 0; (i < usuario.ratings().size()); i++) 
		{
			String nombrePelicula 	= usuario.ratings().get(i).tituloPelicula();
			int calificacion 		= usuario.ratings().get(i).calificacion();
			peliculaYCalificacion.put(nombrePelicula,calificacion);
		}
		return peliculaYCalificacion;
	}

	public void suscribirseA(ArrayList<Genero> unaListaDeGeneros, Usuario unUsuario) 
	{
		Suscripcion unaNuevaSuscripcion	= new Suscripcion(unaListaDeGeneros, unUsuario);
		this.sistema().addObserver(unaNuevaSuscripcion);
	}
	
	//PROPOSITO: Dado un listado de imdbs de peliculas del sistema, retorna las peliculas 
	//			 correspondientes a los imdbs de la lista. 
	public ArrayList<Pelicula> listarPeliculas(ArrayList<String> imdbs) 
	{
		return this.sistema().listarPeliculas(imdbs);
	}
	
}
