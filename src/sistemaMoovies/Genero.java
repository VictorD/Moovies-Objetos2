package sistemaMoovies;

import java.util.ArrayList;

public abstract class Genero 
{
	//Atributos
	private	String nombre;
	
	//Constructor
	public Genero(String unNombre) 
	{
		this.setNombre(unNombre);
	}

	//Getter && Setter
	private String getNombre()
	{
		return this.nombre;
	}
	
	private void setNombre(String unNombreDeGenero)
	{
		this.nombre	= unNombreDeGenero;
	}
	
	//Metodos
	public String nombre()
	{
		return this.getNombre();
	}

	public abstract ArrayList<String> listarGeneros();
}
