package sistemaMoovies;

import java.util.ArrayList;
import java.util.Collections;


public class PeliculasCalificadasCon4OMasPorMasDeLaMitadDeAmigosQueUsuarioAunNoEvaluo implements MetodoDeRecomendacionDePeliculas
{
	//Cuando un usuario selecciona este metodo de recomendacion, se recomendaran aquellas peliculas
	//que hayan sido calificadas con 4 o mas por mas de la mitad de sus amigos y que el usuario aun no haya evaluado.
	
	@Override
	public ArrayList<Pelicula> filtrarRecomendadas(Usuario usuario, ArrayList<Usuario> amigos) 
	{
		ArrayList<Pelicula> peliculasDeUsuarios = new ArrayList<Pelicula>();
	
		ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
		
		for(Usuario amigo : amigos)
		{
			peliculasDeUsuarios.addAll(amigo.peliculasCalificadasConPuntajeMinimo(4));
		}
		
		for(Pelicula pelicula : peliculasDeUsuarios)
		{
			if(Collections.frequency(peliculasDeUsuarios, pelicula ) > (amigos.size()/2) &&
									 !usuario.peliculasCalificadas().contains(pelicula) &&
									 !peliculas.contains(pelicula))
								     
			{	
				peliculas.add(pelicula);
			}
		}
		
		return peliculas;
	}

	
}
