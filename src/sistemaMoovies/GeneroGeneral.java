package sistemaMoovies;

import java.util.ArrayList;

public class GeneroGeneral extends Genero 
{
	//Atributos
	private ArrayList<GeneroEspecifico> subGeneros;
	
	//Constructor
	public GeneroGeneral(String unNombreDeGenero) 
	{
		super(unNombreDeGenero);
		this.setSubGeneros(new ArrayList<GeneroEspecifico>());
	}

	//Getters & Setters
	private void setSubGeneros(ArrayList<GeneroEspecifico> unaListaDeGeneros)
	{
		this.subGeneros	= unaListaDeGeneros;
	}
	
	private ArrayList<GeneroEspecifico> getSubGeneros()
	{
		return this.subGeneros;
	}
	
	//Metodos
	public ArrayList<GeneroEspecifico> subGeneros() 
	{
		return this.getSubGeneros();
	}

	public void agregarSubGenero(GeneroEspecifico unSubGenero) 
	{
		this.getSubGeneros().add(unSubGenero);
	}	
	
	/**
	 * Devuelve una lista de strings donde estan los nombres de todos los subgeneros que tiene incluido su nombre 
	 */
	public ArrayList<String> listarGeneros()
	{
		ArrayList<String> listaDeGeneros	= new ArrayList<String>();
		listaDeGeneros.add(this.nombre());
		
		for (GeneroEspecifico subGenero : this.getSubGeneros()) 
		{
			listaDeGeneros.addAll(subGenero.listarGeneros());			
		}

		return listaDeGeneros;
	}
	
}
