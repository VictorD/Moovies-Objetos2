package sistemaMoovies;

import java.util.Comparator;

/**
 * Clase que implementa la interfaz Comparator, para poder comparar peliculas por su promedio
 * @author Victor Degano
 */
public class OrdenarPeliculaPorPromedio implements Comparator<Pelicula> 
{
	/**
	 * Compara dos peliculas (o1 y o2) por su promedio.
	 * @param o1 - Es la primera pelicula que se va a comparar.
	 * @param o2 - Es la segunda pelicula que se va a comparar.
	 * @return Int 	- Devuelve -1 si el promedio de o1 > promedio de o2 
	 * 				- Devuelve 1 si el promedio de o1 < promedio de o2
	 * 				- Devuelve 0 si el promedio de o1 = promedio de o2
	 * @author Victor Degano
	 */
	@Override
	public int compare(Pelicula o1, Pelicula o2) 
	{
		if (o1.promedio() > o2.promedio())
		{
			return -1;
		}
		else if (o1.promedio() < o2.promedio())
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

}
