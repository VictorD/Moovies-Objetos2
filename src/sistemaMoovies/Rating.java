package sistemaMoovies;

import java.time.LocalDate;

public class Rating
{

	//Atributos
	
	private String nombreUsuario;
	private Integer idUsuario;
	private String tituloPelicula;
	private String imdb;
	private LocalDate fechaDeCalificacion;
	private Integer calificacion;
	private Integer peliculaID;
	
	//Constructores
	public Rating(Integer userID, LocalDate unaFecha, Integer unaPeliculaId, Integer calificacion) 
	{
		this.nombreUsuario			= "";		
		this.idUsuario				= userID;
		this.tituloPelicula			= "";
		this.imdb					= "";
		this.fechaDeCalificacion	= unaFecha;
		this.calificacion			= calificacion;
		this.peliculaID				= unaPeliculaId;
	}

	//Getters && Setters
	private int getidUsuario()
	{
		return this.idUsuario;
	}
		
	private String getNombreUsuario() {
		return this.nombreUsuario;
	}
	
	private String getImdb()
	{
		return this.imdb;
	}
	
	private void setNombreDePelicula(String unNombreDePelicula)
	{
		this.tituloPelicula	= unNombreDePelicula;
	}
	
	private void setNombreDeUsuario(String unNombreDeUsuario)
	{
		this.nombreUsuario	= unNombreDeUsuario;
	}
	
	private int getCalificacion()
	{
		return this.calificacion;
	}
	
	private LocalDate getFechaDeCalificacion() 
	{
		return this.fechaDeCalificacion;
	}
	

	private String getTituloPelicula() {
		return this.tituloPelicula;
	}
	
	private void setPeliculaImdb(String unImdb) 
	{
		this.imdb	= unImdb;
	}
	
	//Metodos
	public String peliculaImdb()
	{
		return this.getImdb();
	}
	
	public void agregarNombreDePelicula(String unNombreDePelicula) 
	{
		this.setNombreDePelicula(unNombreDePelicula);
	}

	public void agregarNombreDeUsuario(String unNombreDeUsuario) 
	{
		this.setNombreDeUsuario(unNombreDeUsuario);
	}

	
	public Integer calificacion()
	{
		return this.getCalificacion();
	}


	public String nombreUsuario() {
		return this.getNombreUsuario();
	}


	public Integer idUsuario() {
		return this.getidUsuario();
	}


	public String tituloPelicula() 
	{
		return this.getTituloPelicula();
	}

	public LocalDate fechaDeCalificacion() 
	{
		return this.getFechaDeCalificacion();
	}


	public int peliculaId() 
	{
		return this.peliculaID;
	}

	public void agregarPeliculaIMDB(String unImdb) 
	{
		this.setPeliculaImdb(unImdb);	
	}
}