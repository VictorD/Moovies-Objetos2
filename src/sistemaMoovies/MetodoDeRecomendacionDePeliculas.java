package sistemaMoovies;

import java.util.ArrayList;


public interface MetodoDeRecomendacionDePeliculas 
{
	ArrayList<Pelicula> filtrarRecomendadas(Usuario usuario, ArrayList<Usuario> amigos);
}
