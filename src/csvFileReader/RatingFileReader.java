package csvFileReader;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import sistemaMoovies.Rating;

public class RatingFileReader extends CSVFileReader<Rating>{

	protected String getSeparator(){
		return " \\| ";
	}
	
	public RatingFileReader(String fileName) {
		super(fileName);
	}

	@Override
	protected Rating parseLine(String[] lineData) 
	{
		LocalDate fechaCalif = Instant.ofEpochSecond(Integer.parseInt(lineData[1]))
				 .atZone(ZoneId.of("UTC"))
				 .toLocalDate();
		
		Rating resultadoRating = new Rating(Integer.parseInt(lineData[0]), fechaCalif, Integer.parseInt(lineData[2]), Integer.parseInt(lineData[3]));
		
		return resultadoRating;
		
	}
	
	
}
