package csvFileReader;

public class Contactos {

	private Integer usuarioID;
	private Integer amigoID;

	public Contactos(Integer usuarioID, Integer amigoID) {
		
		this.usuarioID = usuarioID;
		this.amigoID   = amigoID;
		
	}

	private Integer getUsuarioID()
	{
		return this.usuarioID;
	}
	
	private Integer getAmigoID()
	{
		return this.amigoID;
	}
	
	public Integer usuarioID()
	{
		return this.getUsuarioID();
	}
	
	public Integer amigoID()
	{
		return this.getAmigoID();
	}

	
}
