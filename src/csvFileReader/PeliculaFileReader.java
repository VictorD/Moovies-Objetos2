package csvFileReader;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import sistemaMoovies.ContenedorDePelicula;
import sistemaMoovies.Genero;
import sistemaMoovies.GeneroGeneral;
import sistemaMoovies.Pelicula;


public class PeliculaFileReader extends CSVFileReader<ContenedorDePelicula>{

	protected String getSeparator(){
		return " \\| ";
		}
	
	public PeliculaFileReader(String fileName) {
		super(fileName);
	}
	
public LocalDate fechaDeEstreno(String fecha) {
		
		List<SimpleDateFormat> formatos = new ArrayList<SimpleDateFormat>();
		//formatos.add(new SimpleDateFormat("d-MMM-yyyy"));
		//formatos.add(new SimpleDateFormat("yyyy-MM-dd"));
		formatos.add(new SimpleDateFormat("d-MMM-yy", Locale.US));
		//formatos.add(new SimpleDateFormat("d-m-y"));
		//formatos.add(new SimpleDateFormat("DD-mm-yyyy"));
		//formatos.add(new SimpleDateFormat("d-MMM-YYYY"));
		
		for(SimpleDateFormat formato : formatos)
		{
			String nuevaFecha	=	fecha;
			if(fecha.contains("sept"))
			{
				nuevaFecha	= fecha.replace("sept", "sep");
			}
			try
			{
				Date date = new Date(formato.parse(nuevaFecha).getTime());
				LocalDate fdate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				return fdate;
			} 
			catch (ParseException e) 
			{
				//e.printStackTrace();
			}
		}	
		return null;
	}
	
	@Override
	protected ContenedorDePelicula parseLine(String[] lineData) {
		
		ArrayList<String> generosTotales  = new ArrayList<String>();
		
		ArrayList<String> generosPosibles = new ArrayList<String>();

		ArrayList<Genero> generosDePelicula = new ArrayList<Genero>();
		
		Pelicula peliculaAImportar;
		
		Collections.addAll(generosPosibles, (lineData[4]), 
				   							(lineData[5]), 
				   							(lineData[6]), 
				   							(lineData[7]), 
				   							(lineData[8]), 
				   							(lineData[9]), 
				   							(lineData[10]),
				   							(lineData[11]), 
				   							(lineData[12]), 
				   							(lineData[13]), 
				   							(lineData[14]), 
				   							(lineData[15]), 
				   							(lineData[16]), 
				   							(lineData[17]), 
				   							(lineData[18]), 
				   							(lineData[19]), 
				   							(lineData[20]),
				   							(lineData[21]), 
				   							(lineData[22]));
		
		Collections.addAll(generosTotales, "Unknown",
										   "Action",
										   "Adventure",
										   "Animation",
										   "Children's",
										   "Comedy",
										   "Crime",
										   "Documentary",
										   "Drama",
										   "Fantasy",
										   "Film-Noir",
										   "Horror",
										   "Musical", 
										   "Mistery",
										   "Romance",
										   "Sci-Fi",
										   "Thriller",
										   "War",
										   "Western");

	
		for(int i = 0; i < 19; i++)
		{	
			if(Integer.parseInt(generosPosibles.get(i)) == 1)
			{
				generosDePelicula.add(new GeneroGeneral(generosTotales.get(i)));
			}
		}
							  
		peliculaAImportar	= new Pelicula(lineData[1], fechaDeEstreno(lineData[2]), lineData[3], generosDePelicula);
		int unId	= Integer.parseInt(lineData[0]); 
		ContenedorDePelicula resultadoPelicula = new ContenedorDePelicula(unId , peliculaAImportar );
		
		return resultadoPelicula;
		
	    }

	
}


