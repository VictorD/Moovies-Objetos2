package csvFileReader;

import sistemaMoovies.ContenedorDeUsuario;
import sistemaMoovies.Usuario;

public class UsuarioFileReader extends CSVFileReader<ContenedorDeUsuario>{

	
	protected String getSeparator()
	{
		return "\\|";
	}
	
	public UsuarioFileReader(String fileName) 
	{
		super(fileName);
	}
	
	@Override
	protected ContenedorDeUsuario parseLine(String[] lineData) 
	{
		Usuario usuarioResultado = new Usuario(lineData[5], lineData[6], Integer.parseInt(lineData[1]), lineData[3], lineData[4]);
		ContenedorDeUsuario contenedorUsuarioResultado	= new ContenedorDeUsuario( Integer.parseInt(lineData[0]), usuarioResultado);
		
		return contenedorUsuarioResultado;
	}
	
}
