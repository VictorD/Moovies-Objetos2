package csvFileReader;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sistemaMoovies.Rating;

public class RatingIMDBFileReader  {

	//Clase que toma una lista de ratings y genera un rating IMDB para una pelicula.
	

	//Toma un idPelicula y una lista de calificaciones, calcula el promedio de calificacion
	//de la pelicula.
	public Integer ratingIMDBDePelicula(int id, List<Rating> ratingsAImportar) 
	{
		int sumaDeCalificacion = 0;
		int apariciones = 0;
		
		for(Rating rating : ratingsAImportar)
		{
			if(id == rating.peliculaId())
			{
				apariciones++;
				sumaDeCalificacion = sumaDeCalificacion + rating.calificacion();
			}
		}
		
		return sumaDeCalificacion/apariciones;
	}

	//Toma una lista de calificaciones, asocia cada calificacion promedio a su pelicula correspondiente.
	//retorna un map<idPelicula, ratingIMDB>
	public HashMap<Integer, Integer> cargarRatingsIMDBImportados(List<Rating> ratingsAImportar) 
	{
		HashMap<Integer, Integer> ratingsPeliculas = new HashMap<Integer, Integer>();
		Set<Integer> ids = new HashSet<Integer>();
		
		for(Rating rating : ratingsAImportar)
		{
			ids.add(rating.peliculaId());
		}
		
		for(int id : ids)
		{
			ratingsPeliculas.put(id, this.ratingIMDBDePelicula(id, ratingsAImportar));
		}
		
		return ratingsPeliculas;
	}

	
}







